/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Ronak
 */
public class Manager extends Person {
    
    private int managerID;
    
    private static int count = 0;
    
    public Manager()
    {
        count++;
        managerID = count;
    }

    public int getManagerID() {
        return managerID;
    }

    public void setManagerID(int managerID) {
        this.managerID = managerID;
    }
    
}
