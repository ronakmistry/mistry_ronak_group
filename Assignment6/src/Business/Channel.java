/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Ronak
 */
public class Channel {
    private String channelName;
    private int channelCost;

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public int getChannelCost() {
        return channelCost;
    }

    public void setChannelCost(int channelCost) {
        this.channelCost = channelCost;
    }
    
    
}
