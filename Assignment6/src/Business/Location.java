/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Ronak
 */
public class Location {
    private String locationName;
    private List<Location> subLocations;
    private Market market;
    
    public Location()
    {
        subLocations = new ArrayList<Location>();
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public List<Location> getSubLocations() {
        return subLocations;
    }

    public void setSubLocations(List<Location> subLocations) {
        this.subLocations = subLocations;
    }

    public Market getMarket() {
        return market;
    }

    public void setMarket(Market market) {
        this.market = market;
    }
    
    public Location addSubLocation(Location loc,String locName)
    {
        Location l = new Location();
        l.setLocationName(locName);
        loc.getSubLocations().add(l);
        return l;
    }
    
    public void removeSubLocation(Location loc)
    {
        subLocations.remove(loc);
    }
}
