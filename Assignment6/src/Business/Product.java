/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Ronak
 */
public class Product {
    
    private String prodName;
    private int prodID;
    private int price;
    private int prodAvailability;
    
    private static int count = 0;
    
    public Product()
    {
        count++;
        prodID = count;
    }

    public String getProdName() {
        return prodName;
    }

    public void setProdName(String prodName) {
        this.prodName = prodName;
    }

    public int getProdID() {
        return prodID;
    }

    public void setProdID(int prodID) {
        this.prodID = prodID;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getProdAvailability() {
        return prodAvailability;
    }

    public void setProdAvailability(int prodAvailability) {
        this.prodAvailability = prodAvailability;
    }
    
    @Override
    public String toString() {
        return prodName; //To change body of generated methods, choose Tools | Templates.
    }
    
}
