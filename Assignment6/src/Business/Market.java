/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Ronak
 */
public class Market {
    private String marketName;
    private String marketType;
    private List<Channel> channelUsed;
    private List<Market> subMarket;
    
    public Market()
    {
        subMarket = new ArrayList<Market>();
    }

    public String getMarketName() {
        return marketName;
    }

    public void setMarketName(String marketName) {
        this.marketName = marketName;
    }

    public String getMarketType() {
        return marketType;
    }

    public void setMarketType(String marketType) {
        this.marketType = marketType;
    }

    public List<Channel> getChannelUsed() {
        return channelUsed;
    }

    public void setChannelUsed(List<Channel> channelUsed) {
        this.channelUsed = channelUsed;
    }
    
    public List<Market> getSubMarket() {
        return subMarket;
    }

    public void setSubMarket(List<Market> subMarket) {
        this.subMarket = subMarket;
    }
    
    public Market addSubMarket(Market mParent, String name, String type, Channel channel)
    {
        Market mChild = new Market();
        mChild.setMarketName(name);
        mChild.setMarketType(type);
        mChild.getChannelUsed().add(channel);
        mParent.getSubMarket().add(mChild);
        return mChild;
    }
    
    public void addChannel(Channel c)
    {
        channelUsed.add(c);
    }
    
    public void removeSubMarket(Market m)
    {
        subMarket.remove(m);
    }
    
    public void removeSubChannel(Channel c)
    {
        channelUsed.remove(c);
    }
}
