/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Ronak
 */
public class ProductPackage {
    
    private List<Product> productPackage;
    
    public ProductPackage()
    {
        productPackage = new ArrayList<Product>();
    }
    
    public Product addProduct(String prodName, int price, int avail){
        Product p = new Product();
        productPackage.add(p);
        p.setProdName(prodName);
        p.setPrice(price);
        p.setProdAvailability(avail);
        return p;
    }
    
    public void removeProduct(Product p){
        productPackage.remove(p);
    }
    
    public Product searchProduct(int id){
        for (Product product : productPackage) {
            if(product.getProdID()==id){
                return product;
            }
        }
        return null;
    }
}
