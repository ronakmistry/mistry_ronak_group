/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Surabhi Patil
 */
public class VitalSignsHistory {
    
    private ArrayList<VitalSigns> vitalSignHistory;

    public VitalSignsHistory()
    {
        vitalSignHistory = new ArrayList<VitalSigns>();
    }
    //Function to add vital signs
    public VitalSigns addVitalSigns(float cholesterol, float hdl, boolean isSmoker, boolean isDiabetic, int systBP, boolean hasBP)
    {
        VitalSigns vs = new VitalSigns();
        vs.setCholesterol(cholesterol);
        vs.setHdl(hdl);
        vs.setIsSmoker(isSmoker);
        vs.setIsDiabetic(isDiabetic);
        vs.setSystBP(systBP);
        vs.setHasBloodPressure(hasBP);
        vitalSignHistory.add(vs);
        return vs;
    }

    public ArrayList<VitalSigns> getVitalSignHistory() {
        return vitalSignHistory;
    }

    public void setVitalSignHistory(ArrayList<VitalSigns> vitalSignHistory) {
        this.vitalSignHistory = vitalSignHistory;
    }
    //function to remove vital signs
    public void removeVitalSigns(VitalSigns vs)
    {
        vitalSignHistory.remove(vs);
    }
    //function to search vital signs
    public VitalSigns searchVitalSigns()
    {
        for (VitalSigns vs: vitalSignHistory)
        {
            return vs;
        }
        return null;
    }
    
    //Function to calculate mean of all vital signs for a person
    public VitalSigns getMeanVitals(Person person)
    {
        int systBP = 0;
        float hdl = 0F;
        float cholestrol = 0F;
        boolean isSmoker = false;
        boolean isDiabetic = false;
        int count = 0;
        
        for (VitalSigns vs: person.getPatient().getVsh().getVitalSignHistory())
        {
            hdl += vs.getHdl();
            cholestrol += vs.getCholesterol();
            systBP += vs.getSystBP();
            isSmoker = isSmoker || vs.isIsSmoker();
            isDiabetic = isDiabetic || vs.isIsDiabetic();
            count++;
        }
        float meanHdl = hdl/count;
        float meanCholestrol = cholestrol/count;
        int meanSyst = systBP/count;
        
        VitalSigns meanVitals = new VitalSigns();
        meanVitals.setCholesterol(meanCholestrol);
        meanVitals.setHdl(meanHdl);
        meanVitals.setSystBP(meanSyst);
        meanVitals.setIsSmoker(isSmoker);
        meanVitals.setIsDiabetic(isDiabetic);
        
        return meanVitals;
    }
    
    
}
