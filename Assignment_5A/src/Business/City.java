/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Surabhi Patil
 */
public class City {
    
    private CommunityList communityList;
    private String cityName;

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }
    
    public City()
    {
        communityList = new CommunityList();
    }

    public CommunityList getCommunityList() {
        return communityList;
    }
    public void peopleWithHeartDisease() {
        int count = 0;
        for (Community community : communityList.getCommunityList()) {
            for (House house : community.getHouseList().getHouseList()) {
                for (Family family : house.getFamilyList().getFamilyList()) {
                    for (Person person : family.getPersonList().getPersonList()) {
                        int avgRisk = person.getPatient().getAverageRisk();
                        int riskPercent = person.getPatient().getRiskPercent();
                        if (avgRisk < riskPercent) {
                            count++;
                        }
                    }
                }
            }
        }
        System.out.println("People with heart disease are: " + count);
    }

    public void healthyPeoplepercent() {
        float count = 0;
        float totalPeople = 0;
        float percent = 0;
        for (Community community : communityList.getCommunityList()) {
            for (House house : community.getHouseList().getHouseList()) {
                for (Family family : house.getFamilyList().getFamilyList()) {
                    for (Person person : family.getPersonList().getPersonList()) {
                        totalPeople++;
                        int avgRisk = person.getPatient().getAverageRisk();
                        int riskPercent = person.getPatient().getRiskPercent();
                        if (avgRisk > riskPercent) {
                            count++;
                        }
                    }
                }
            }
        }
        percent = (count / totalPeople) * 100;
        percent = Math.round(percent * 100);
        percent = Math.round(percent);
        percent = percent / 100;
        System.out.println("Healthy people percent: " + percent + "%");
    }

    public void diabeticPeopleAgeGroup() {
        int children = 0;
        int seniors = 0;
        int adults = 0;
        for (Community community : communityList.getCommunityList()) {
            for (House house : community.getHouseList().getHouseList()) {
                for (Family family : house.getFamilyList().getFamilyList()) {
                    for (Person person : family.getPersonList().getPersonList()) {
                        if (person.getAge() < 40) {
                            VitalSigns vitalSign = person.getPatient().getVsh().getMeanVitals(person);
                            if (vitalSign.isIsDiabetic() == true) {
                                children++;
                            }
                        }
                        if (person.getAge() > 40 && person.getAge() < 60) {
                            VitalSigns vitalSign = person.getPatient().getVsh().getMeanVitals(person);
                            if (vitalSign.isIsDiabetic() == true) {
                                adults++;
                            }
                        }
                        if (person.getAge() > 60) {
                            VitalSigns vitalSign = person.getPatient().getVsh().getMeanVitals(person);
                            if (vitalSign.isIsDiabetic() == true) {
                                seniors++;
                            }
                        }
                    }
                }
            }
        }
        System.out.println("Children: " + children + "\nAdults: " + adults + "\nSeniors: " + seniors);
    }

    public void ratioOfSmokers() {
        int smokers = 0;
        int nonSmokers = 0;
        for (Community community : communityList.getCommunityList()) {
            for (House house : community.getHouseList().getHouseList()) {
                for (Family family : house.getFamilyList().getFamilyList()) {
                    for (Person person : family.getPersonList().getPersonList()) {
                        VitalSigns vitalSign = person.getPatient().getVsh().getMeanVitals(person);
                        if (vitalSign.isIsSmoker() == true) {
                            smokers++;
                        } else {
                            nonSmokers++;
                        }
                    }
                }
            }
        }
        System.out.println(smokers + ":" + nonSmokers);
    }
}
