/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Surabhi Patil
 */
public class CommunityList {
    
    private ArrayList<Community> communityList;
    
    public CommunityList()
    {
        communityList = new ArrayList<Community>();
    }
    
    
    
    public ArrayList<Community> getCommunityList() {
        return communityList;
    }

    public void setCommunityList(ArrayList<Community> communityList) {
        this.communityList = communityList;
    }
    
    //Function to add a community
    public Community addCommunity(String comName, int comID)
    {
        Community c = new Community();
        c.setComName(comName);
        c.setComID(comID);
        communityList.add(c);
        return c;
    }
    
    public void removeCommunity(Community c)
    {
        communityList.remove(c);
        
    }
    
    public void hasHighBp(int id) {
        for (Community community : communityList) {
            int comID = community.getComID();
            if (comID == id) 
            {
                int count = 0;
                int total = 0;
                for (House house : community.getHouseList().getHouseList()) {
                    for (Family family : house.getFamilyList().getFamilyList()) {
                        for (Person person : family.getPersonList().getPersonList()) {
                            total++;
                            VitalSigns vs = person.getPatient().getVsh().getMeanVitals(person);
                            if (vs.isHasBloodPressure() == true) {
                                count++;
                            }
                        }
                    }
                }
                System.out.println("No of people in community " + community.getComName()+ " suffering from high Bp: " + count + "/" + total + "\n\n");
            }
        }
    }

    public void hasHighCholestrol(int id) {
        for (Community community : communityList) {
            int comID = community.getComID();
            if (comID == id) 
            {
                float men = 0;
                int mentotal = 0;
                float women = 0;
                int womentotal = 0;
                for (House house : community.getHouseList().getHouseList()) {
                    for (Family family : house.getFamilyList().getFamilyList()) {
                        for (Person person : family.getPersonList().getPersonList()) {
                            VitalSigns vs = person.getPatient().getVsh().getMeanVitals(person);
                            if (person.getGender().equalsIgnoreCase("male")) {
                                mentotal++;
                                if (vs.getCholesterol()> 6.22) {
                                    men++;
                                }
                            } else {
                                womentotal++;
                                if (vs.getCholesterol()> 6.22) {
                                    women++;
                                }
                            }
                        }
                    }
                }
                men = (men / mentotal) * 100;
                women = (women / womentotal) * 100;
                System.out.println("Percent men suffering from High Cholestrol: " + men + "%\n Percent women suffering from High Choestrol: " + women + "%\n\n");
            }
        }
    }

    public void ageSmokerGroup(int id) {
        for (Community community : communityList) {
            int comID = community.getComID();
            if (comID == id) { 
                int young = 0;
                int parent = 0;
                int elder = 0;
                for (House house : community.getHouseList().getHouseList()) {
                    for (Family family : house.getFamilyList().getFamilyList()) {
                        for (Person person : family.getPersonList().getPersonList()) {
                            VitalSigns vs = person.getPatient().getVsh().getMeanVitals(person);
                            if (vs.isIsSmoker() == true) {
                                if (person.getAge() >= 31 && person.getAge() <= 40) {
                                    young++;
                                } else if (person.getAge() >= 41 && person.getAge() <= 60) {
                                    parent++;
                                } else {
                                    elder++;
                                }
                            }
                        }
                    }
                }
                if (young < parent) {
                    if (parent < elder) {
                        System.out.println("Age group with max. No. of Smokers is (61-100) with " + elder + " smokers\n\n");
                    } else {
                        System.out.println("Age group with max. No. of Smokers is (41-60) with " + parent + " smokers\n\n");
                    }
                } else if (young < elder) {
                    if (elder < parent) {
                        System.out.println("Age group with max. No. of Smokers is (41-60) with " + parent + " smokers\n\n");
                    } else {
                        System.out.println("Age group with max. No. of Smokers is (61-100) with " + elder + " smokers\n\n");
                    }
                } else {
                    System.out.println("Age group with max. No. of Smokers is (31-40) with " + young + " smokers\n\n");
                }
            }
        }
    }
    
}
