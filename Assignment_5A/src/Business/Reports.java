/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.Scanner;

/**
 *
 * @author Surabhi Patil
 */
public class Reports {
    
    //City level reports
    public void cityReports(City city)
    {
        System.out.println("How many people in city has risk of getting heart disease?");
        city.peopleWithHeartDisease();
        System.out.println("Percent of healthy people in city");
        city.healthyPeoplepercent();
        System.out.println("Diabetic number of people in each age group ");
        city.diabeticPeopleAgeGroup();
        System.out.println("Ratio of smokers to non-smokers in city");
        city.ratioOfSmokers();   
    }
    //Community level reports
    public void communityReports(City city) {
        boolean communityExist = false;
        System.out.println("Enter the community ID");
        Scanner sc = new Scanner(System.in);
        String input = sc.nextLine();
        String id[] = new String[10];
        
            System.out.println("How many people in the community have high BP ?");
            city.getCommunityList().hasHighBp(Integer.parseInt(input));

            System.out.println("What percent of men and women have high cholestrol ?");
            city.getCommunityList().hasHighCholestrol(Integer.parseInt(input));

            System.out.println("Which age group has max Number of smokers ?");
            city.getCommunityList().ageSmokerGroup(Integer.parseInt(input));
        
    }
    
    //House level reports for all houses
    public void houseReports(City city, int hoID)
    {
        for(Community community: city.getCommunityList().getCommunityList())
        {
            for(House house:community.getHouseList().getHouseList())
            {
                System.out.println("1. How many people are diabetic?");
                community.getHouseList().hDiabetic(city, hoID);
                System.out.println("2. How many poeple have high cholesterol?");
                community.getHouseList().hCholesterol(city, hoID);
            }
        }
        
    }

    public void familyReports(City city) {

        
        System.out.println("Please enter Family id");
        Scanner scan = new Scanner(System.in);
        int id = scan.nextInt();

        
        for (Community community : city.getCommunityList().getCommunityList()) {
            for (House house : community.getHouseList().getHouseList()) {
                for(Family family: house.getFamilyList().getFamilyList()){
                    //if (family.getfId() == id)
                    //{
                        System.out.println("\nPerson with highest risk in the family");
                        house.getFamilyList().highRiskPercentPerson( id);
                        System.out.println("\nRatio of diabetic to non-diabetic people in this family");
                        house.getFamilyList().maxRisk(city, id);
                    //}
                
                }

            }
        }


    }
    //Person level reports
    public void personReports(City city, int id) 
    {
        for (Community community : city.getCommunityList().getCommunityList()) 
        {
            for (House house : community.getHouseList().getHouseList()) 
            {
                for (Family family : house.getFamilyList().getFamilyList()) 
                {
                    for (Person person : family.getPersonList().getPersonList()) 
                    {
                        if (person.getPersonID()==id)
                        {
                            System.out.println("Name:" + person.getName() + "\n Risk Score:" + person.getPatient().getRiskPercent() + "\n");
                            System.out.println("Risk Score deviation from average Risk Score:" + (person.getPatient().getRiskPercent() - person.getPatient().getAverageRisk()) + "\n");
                            VitalSigns meanVitalSign = person.getPatient().getVsh().getMeanVitals(person);
                            System.out.println("Total Cholestrol:" + meanVitalSign.getCholesterol());
                            System.out.println("HDL Cholestrol:" + meanVitalSign.getHdl());
                            System.out.println("Systolic Blood Pressure:" + meanVitalSign.getSystBP());
                            System.out.println("Smoker:" + meanVitalSign.isIsSmoker());
                            System.out.println("Diabetic:" + meanVitalSign.isIsDiabetic());
                        }
                    }
                }
            }

        }
    }

}