/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.TreeMap;

/**
 *
 * @author Surabhi Patil
 */
public class VitalSigns {
    
    private float cholesterol;
    private float hdl;
    private boolean isSmoker;
    private boolean isDiabetic;
    private int systBP;
    private boolean hasBloodPressure;

    public boolean isHasBloodPressure() {
        return hasBloodPressure;
    }

    public void setHasBloodPressure(boolean hasBloodPressure) {
        this.hasBloodPressure = hasBloodPressure;
    }
    
    

    public float getCholesterol() {
        return cholesterol;
    }

    public void setCholesterol(float cholesterol) {
        this.cholesterol = cholesterol;
    }

    public float getHdl() {
        return hdl;
    }

    public void setHdl(float hdl) {
        this.hdl = hdl;
    }

    public boolean isIsSmoker() {
        return isSmoker;
    }

    public void setIsSmoker(boolean isSmoker) {
        this.isSmoker = isSmoker;
    }

    public boolean isIsDiabetic() {
        return isDiabetic;
    }

    public void setIsDiabetic(boolean isDiabetic) {
        this.isDiabetic = isDiabetic;
    }

    public int getSystBP() {
        return systBP;
    }

    public void setSystBP(int systBP) {
        this.systBP = systBP;
    }
    
}