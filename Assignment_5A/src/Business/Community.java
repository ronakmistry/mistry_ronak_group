/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Surabhi Patil
 */
public class Community {
    private String comName;
    private int comID;
    private HouseList houseList;

    
    public Community()
    {
        houseList = new HouseList();
    }

    public int getComID() {
        return comID;
    }

    public void setComID(int comID) {
        this.comID = comID;
    }
    
    
    public String getComName() {
        return comName;
    }

    public void setComName(String comName) {
        this.comName = comName;
    }

    public HouseList getHouseList() {
        return houseList;
    }


    
    
}
