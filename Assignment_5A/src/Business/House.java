/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Surabhi Patil
 */
public class House {
    
    private String hName;
    private int hID;
    private FamilyList familyList;
    
    public House()
    {
        familyList = new FamilyList();
    }

    public int gethID() {
        return hID;
    }

    public void sethID(int hID) {
        this.hID = hID;
    }
    
    

    public String gethName() {
        return hName;
    }

    public void sethName(String hName) {
        this.hName = hName;
    }

    public FamilyList getFamilyList() {
        return familyList;
    }
    
}
