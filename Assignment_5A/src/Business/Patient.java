/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.TreeMap;

/**
 *
 * @author Surabhi Patil
 */
public class Patient {
    
    private int patientID;
    private int riskPercent;
    private int averageRisk;
    private VitalSignsHistory vsh;
    
    public Patient()
    {
        vsh = new VitalSignsHistory();
    }

    public int getRiskPercent() {
        return riskPercent;
    }

    public void setRiskPercent(int riskPercent) {
        this.riskPercent = riskPercent;
    }

    public int getAverageRisk() {
        return averageRisk;
    }

    public void setAverageRisk(int averageRisk) {
        this.averageRisk = averageRisk;
    }

    
    
    public int getPatientID() {
        return patientID;
    }

    public void setPatientID(int patientID) {
        this.patientID = patientID;
    }

    public VitalSignsHistory getVsh() {
        return vsh;
    }

    public void setVsh(VitalSignsHistory vsh) {
        this.vsh = vsh;
    }
    
    //Function to calculate Risk percent based on Total Risk Points
    private int getRiskValue(int total, Person person) {
        TreeMap<Integer, Integer> map = new TreeMap<Integer, Integer>();
        if (person.getGender() == "male") {
            map.put(-50, 2);
            map.put(-1, 2);
            map.put(0, 3);
            map.put(1, 3);
            map.put(2, 4);
            map.put(3, 5);
            map.put(4, 7);
            map.put(5, 8);
            map.put(6, 10);
            map.put(7, 13);
            map.put(8, 16);
            map.put(9, 20);
            map.put(10, 25);
            map.put(11, 31);
            map.put(12, 37);
            map.put(13, 45);
            map.put(14, 53);
            map.put(50, 53);
        } else {
            map.put(-50, 1);
            map.put(-2, 1);
            map.put(-1, 2);
            map.put(0, 2);
            map.put(1, 2);
            map.put(2, 3);
            map.put(3, 3);
            map.put(4, 4);
            map.put(5, 4);
            map.put(6, 5);
            map.put(7, 6);
            map.put(8, 7);
            map.put(9, 8);
            map.put(10, 10);
            map.put(11, 11);
            map.put(12, 13);
            map.put(13, 15);
            map.put(14, 18);
            map.put(15, 20);
            map.put(16, 24);
            map.put(17, 27);
            for(int i=18; i<28;i++)
            map.put(i, i+10);
        }
        int lookingFor = total;
        int groupBelow = map.floorEntry(lookingFor).getValue();
        //System.out.println(total +" tot "+ groupBelow);
        return groupBelow;

    }


    //Function to display Average risk percent by age
    private int getAverageAgeRiskPercent(Person person) {
        TreeMap<Integer, Integer> map = new TreeMap<Integer, Integer>();
        if (person.getGender() == "male") {
            map.put(30, 3);
            map.put(34, 3);
            map.put(35, 5);
            map.put(39, 5);
            map.put(40, 7);
            map.put(44, 7);
            map.put(45, 11);
            map.put(49, 11);
            map.put(50, 14);
            map.put(54, 14);
            map.put(55, 16);
            map.put(59, 16);
            map.put(60, 21);
            map.put(64, 21);
            map.put(65, 25);
            map.put(69, 25);
            map.put(70, 30);
            map.put(74, 30);
            map.put(75, 40);
            map.put(100, 40);
        } else {
            map.put(30, 1);
            map.put(34, 1);
            map.put(35, 1);
            map.put(39, 1);
            map.put(40, 2);
            map.put(44, 2);
            map.put(45, 5);
            map.put(49, 5);
            map.put(50, 8);
            map.put(54, 8);
            map.put(55, 12);
            map.put(59, 12);
            map.put(60, 12);
            map.put(64, 12);
            map.put(65, 13);
            map.put(69, 13);
            map.put(70, 14);
            map.put(74, 14);
            map.put(75, 18);
            map.put(100, 18);

        }
        int lookingFor = person.getAge();
        int groupBelow = map.floorEntry(lookingFor).getValue();
         // System.out.println(person.getAge() +" age  "+ groupBelow);
        return groupBelow;

    }
    //Function to set Risk Percent and Average Risk
    public void calculateRisk(int total, Person person) {

        int riskPercent = getRiskValue(total, person);
        person.getPatient().setRiskPercent(riskPercent);
        int averageAgeRiskPercent = getAverageAgeRiskPercent(person);
        person.getPatient().setAverageRisk(averageAgeRiskPercent);

    }
    
    //Function to calculate Risk points as per Age
    public int calcAgeRiskPoints(Person person) {
        TreeMap<Integer, Integer> map = new TreeMap<Integer, Integer>();
        if (person.getGender() == "male") {
            map.put(30, -1);
            map.put(34, -1);
            map.put(35, 0);
            map.put(39, 0);
            map.put(40, 1);
            map.put(44, 1);
            map.put(45, 2);
            map.put(49, 2);
            map.put(50, 3);
            map.put(54, 3);
            map.put(55, 4);
            map.put(59, 4);
            map.put(60, 5);
            map.put(64, 5);
            map.put(65, 6);
            map.put(69, 6);
            map.put(70, 7);
            map.put(74, 7);
            map.put(75, 8);
            map.put(100, 8);
        } else {
            map.put(30, -9);
            map.put(34, -9);
            map.put(35, -4);
            map.put(39, -4);
            map.put(40, 0);
            map.put(44, 0);
            map.put(45, 3);
            map.put(49, 3);
            map.put(50, 6);
            map.put(54, 6);
            map.put(55, 7);
            map.put(59, 7);
            map.put(60, 8);
            map.put(64, 8);
            map.put(65, 8);
            map.put(69, 8);
            map.put(70, 8);
            map.put(74, 8);
            map.put(75, 8);
            map.put(100, 8);

        }
        int lookingFor = person.getAge();
        int groupBelow = map.floorEntry(lookingFor).getValue();
        return groupBelow;

    }

    //Function to calculate risk points as per cholestrol
    public int calcCholestroolRiskPoints(VitalSigns meanVitalSign, Person person) {
        TreeMap<Float, Integer> map = new TreeMap<Float, Integer>();
        if (person.getGender() == "male") {
            map.put(3.6F, -3);
            map.put(4.14F, -3);
            map.put(4.15F, 0);
            map.put(5.17F, 0);
            map.put(5.18F, 1);
            map.put(6.21F, 1);
            map.put(6.22F, 2);
            map.put(7.24F, 2);
            map.put(7.25F, 3);
            map.put(10.3F, 3);
        } else {
            map.put(3.6F, -2);
            map.put(4.14F, -2);
            map.put(4.15F, 0);
            map.put(5.17F, 0);
            map.put(5.18F, 1);
            map.put(6.21F, 1);
            map.put(6.22F, 1);
            map.put(7.24F, 1);
            map.put(7.25F, 3);
            map.put(10.3F, 3);
        }
        Float lookingFor = meanVitalSign.getCholesterol();
        int groupBelow = map.floorEntry(lookingFor).getValue();
        //System.out.println(meanVitalSign.getTotalCholesterol() +" chol  "+ groupBelow);
        return groupBelow;
    }

    //Function to calculate risk points as per HDL
    public int getHdlCholestrolRiskPoints(VitalSigns meanVitalSign, Person person) {
        TreeMap<Float, Integer> map = new TreeMap<Float, Integer>();
        if (person.getGender() == "male") {
            map.put(0.8F, 2);
            map.put(0.90F, 2);
            map.put(0.91F, 1);
            map.put(1.16F, 1);
            map.put(1.17F, 0);
            map.put(1.29F, 0);
            map.put(1.30F, 0);
            map.put(1.55F, 0);
            map.put(1.56F, -2);
            map.put(4.0F, -2);
        } else {
            map.put(0.8F, 5);
            map.put(0.90F, 5);
            map.put(0.91F, 2);
            map.put(1.16F, 2);
            map.put(1.17F, 1);
            map.put(1.29F, 1);
            map.put(1.30F, 0);
            map.put(1.55F, 0);
            map.put(1.56F, -3);
            map.put(4.0F, -3);
        }
        Float lookingFor = meanVitalSign.getHdl();
        int groupBelow = map.floorEntry(lookingFor).getValue();
         // System.out.println(meanVitalSign.getHdlCholesterol() +" hdl  "+ groupBelow);
        return groupBelow;
    }

    //Function to calculate risk points as per Blood Pressure
    public int getsystolicBloodPressureRiskPoints(VitalSigns meanVitalSign, Person person) {
        TreeMap<Integer, Integer> map = new TreeMap<Integer, Integer>();
        if (person.getGender() == "male") {
            map.put(90, 0);
            map.put(119, 0);
            map.put(120, 0);
            map.put(129, 0);
            map.put(130, 1);
            map.put(139, 1);
            map.put(140, 2);
            map.put(159, 2);
            map.put(160, 3);
            map.put(200, 3);
        } else {
            map.put(90, -3);
            map.put(119, -3);
            map.put(120, 0);
            map.put(129, 0);
            map.put(130, 0);
            map.put(139, 0);
            map.put(140, 2);
            map.put(159, 2);
            map.put(160, 3);
            map.put(200, 3);

        }

        Integer lookingFor = meanVitalSign.getSystBP();
        int groupBelow = map.floorEntry(lookingFor).getValue();
        //   System.out.println(meanVitalSign.getSystolicBloodPressure() +" sysbp "+ groupBelow);
        return groupBelow;
    }

    //Function to calculate risk points as per Diabetes Status
    public int getDiabetesRiskPoints(VitalSigns meanVitalSign, Person person) {
        TreeMap<Boolean, Integer> map = new TreeMap<Boolean, Integer>();
        if (person.getGender() == "male") {
            map.put(false, 0);
            map.put(true, 2);
        } else {
            map.put(false, 0);
            map.put(true, 4);
        }
        boolean lookingFor = meanVitalSign.isIsDiabetic();
        int groupBelow = map.floorEntry(lookingFor).getValue();
          //System.out.println(meanVitalSign.isIsDiabetic() +" dia "+ groupBelow);
        return groupBelow;
    }

    //Function to calculate risk points as per Smoker status
    public int getSmokerRiskPoints(VitalSigns meanVitalSign, Person person) {
        TreeMap<Boolean, Integer> map = new TreeMap<Boolean, Integer>();

        map.put(false, 0);
        map.put(true, 2);

        boolean lookingFor = meanVitalSign.isIsSmoker();
        int groupBelow = map.floorEntry(lookingFor).getValue();
        // System.out.println(meanVitalSign.isIsSmoker() +" smo "+ groupBelow);
        return groupBelow;
    }
    
}
