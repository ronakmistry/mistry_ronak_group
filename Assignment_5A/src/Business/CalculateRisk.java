/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;
import Business.Person;
import Business.VitalSigns;
import java.util.Scanner;
/**
 *
 * @author Surabhi Patil
 */
public class CalculateRisk {
    
    public CalculateRisk()
    {
        createPerson();
    }
    
    //Function used to create a person
    public void createPerson() {
     
        String name = "";
        String gender = "";
        float totalCholesterol = 0F;
        float hdlCholesterol = 0F;
        int systolicPressure = 0;
        boolean isSmoker = false;
        boolean isDiabetic = false;
        boolean isBP = false;
        
        int flag = 0;
        
        Scanner sc = new Scanner(System.in);
            //Request user for name
            System.out.println("Please enter your personal details:");
            System.out.println("Enter your name:");
            
            
            name = sc.nextLine();
            
            //Validate name
            while(name.trim().matches(".*\\d+.*") || name.trim().equals("") )
            {
                System.out.println("Please enter valid name:");
                name = sc.nextLine();
            }
            
            //Request user for age
            System.out.println("Enter your age:");
            
            //Validate age
            while(!sc.hasNextInt()) {
                
                System.out.println("Please enter valid age:");
                sc.next();
            }
            
            int  age2 = sc.nextInt();
            
            //Validate age
            while(age2 < 30 || age2 > 100) {
                System.out.println("Age must be between 30 and 100:");
                while(!sc.hasNextInt()) {
                
                System.out.println("Please enter valid age:");
                sc.next();
            }
            
            age2 = sc.nextInt();
            
            }
            //Request user for gender
            System.out.println("Please select your gender:\n1.Male 2.Female");
            
            //Validate Gender
            while(!sc.hasNextInt()) {
                System.out.println("Please enter valid option:\n1.Male 2.Female");
                sc.next();
   
            }
            flag = sc.nextInt();
            
            //Validate gender
            while(flag < 1 || flag > 2) {
                System.out.println("Please enter valid option:\n1.Male 2.Female");
                while(!sc.hasNextInt()) {
                System.out.println("Please enter valid option:\n1.Male 2.Female");
                sc.next();
   
            }
            flag = sc.nextInt();
            }
            //Set gender
            if(flag==1)
                gender = "male";
            else
                gender = "female";
            
            //Request user for Vital Signs
            //Request user for cholestrol
            System.out.println("\nPlease enter your vital signs details:");
            System.out.println("Total Cholesterol:");
            
            //Validate cholestrol entry
            while(!sc.hasNextFloat()) {
                System.out.println("Please enter valid value of Total cholesterol");
                sc.next();
            }
            totalCholesterol = sc.nextFloat();
            
            //validate cholestrol
            while(totalCholesterol < 3.59 || totalCholesterol > 10.7) 
            {
                System.out.println("Total cholesterol must be between 3.6 to 10.6:");
                while(!sc.hasNextFloat()) {
                System.out.println("Please enter valid value of Total cholesterol");
                sc.next();
            }
            totalCholesterol = sc.nextFloat();
            }
            
            totalCholesterol = Math.round(totalCholesterol*10);
            totalCholesterol = totalCholesterol/10;
            
            //Request user for HDL Cholestrol
            System.out.println("HDL Cholesterol:");
            
            while(!sc.hasNextFloat()) {
                System.out.println("Please enter valid value of HDL cholesterol");
                sc.next();
            }
            hdlCholesterol = sc.nextFloat();
            
            //Validate HDL value
            while(hdlCholesterol < 0.79 || hdlCholesterol > 4.1) 
            {
                System.out.println("HDL cholesterol must be between 0.8 to 4.0:");
                while(!sc.hasNextFloat()) {
                System.out.println("Please enter valid value of HDL cholesterol");
                sc.next();
            }
            hdlCholesterol = sc.nextFloat();
            }
            
            hdlCholesterol = Math.round(hdlCholesterol*10);
            hdlCholesterol = hdlCholesterol/10;
            
            //Request user for systolic blood pressure
            System.out.println("Systolic Blood Pressure:");
            
            while (!sc.hasNextInt()) {
                System.out.println("Please enter valid value of Systolic pressure");
                sc.next();
            }
            
            systolicPressure = sc.nextInt();
            
            //validate blood pressure
            while(systolicPressure < 90 || systolicPressure > 200)
            {
                System.out.println("Systolic Pressure must be between 90 to 200:");
                while (!sc.hasNextInt()) {
                System.out.println("Please enter valid value of Systolic pressure");
                sc.next();
            }
            
            systolicPressure = sc.nextInt();
            }
            
            //Request if user is smoker
            System.out.println("Do you smoke:\n1.Yes 2.No");
            
            while(!sc.hasNextInt()) {
                System.out.println("Please enter valid option:\n1.Yes 2.No");
                sc.next();
            }
            flag = Integer.parseInt(sc.next());
            
            //Validate entry
            while(flag < 1 || flag > 2) {
                System.out.println("Please enter valid option:\n1.Yes 2.No");
                while(!sc.hasNextInt()) {
                System.out.println("Please enter valid option:\n1.Yes 2.No");
                sc.next();
            }
            flag = Integer.parseInt(sc.next());
            }
            
            if(flag == 1)
                isSmoker = true;
            
            
            
            //Request if user is Diabetic
            System.out.println("Diabetic:\n1.Yes 2.No");
            
            while(!sc.hasNextInt()) {
                System.out.println("Please enter valid option:\n1.Yes 2.No");
                sc.next();
   
            }
            flag = sc.nextInt();
            
            //validate entry
            while(flag < 1 || flag > 2) {
                System.out.println("Please enter valid option:\n1.Yes 2.No");
                while(!sc.hasNextInt()) {
                System.out.println("Please enter valid option:\n1.Yes 2.No");
                sc.next();
   
            }
            flag = sc.nextInt();
            }
            
            if(flag == 1)
                isDiabetic = true;
            
        
            //Initialize Person
            Person person = new Person();
            //Initialize Patient
            Patient patient = new Patient();
            person.setName(name);
            person.setAge(age2);
            person.setGender(gender);
            person.setPatient(patient);
        
            //Initialize and set Vital Signs for Person
            VitalSigns vitalSigns= person.getPatient().getVsh().addVitalSigns(totalCholesterol, hdlCholesterol, isSmoker, isDiabetic, systolicPressure, isBP);
            //Calculate total Risk Points
            int risk = person.calculateRiskPointTotal(person);
            //Calculate Risk Percent
            person.getPatient().calculateRisk(risk, person);
            //Display Risk Percent for user
            System.out.println("Your risk percent is: " + person.getPatient().getRiskPercent());
    }
   
}


