/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.TreeMap;

/**
 *
 * @author Surabhi Patil
 */
public class Person {
    
    private int PersonID;
    private String name;
    private String gender;
    private int age;
    private Patient patient;

    public int getPersonID() {
        return PersonID;
    }

    public void setPersonID(int PersonID) {
        this.PersonID = PersonID;
    }
    
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }
    
    //Function to calculate the total Risk points for a person
    public int calculateRiskPointTotal(Person person)
    {
        int risk = 0;
            
        VitalSigns vs = person.getPatient().getVsh().getMeanVitals(person);
        
        boolean isSmoker = vs.isIsSmoker();
        boolean isDiabetic = vs.isIsDiabetic();
        
        int ageRisk = person.getPatient().calcAgeRiskPoints(person);
        int systBPRisk = person.getPatient().getsystolicBloodPressureRiskPoints(vs, person);
        int hdlRisk = person.getPatient().getHdlCholestrolRiskPoints(vs, person);
        int cholestrolRisk = person.getPatient().calcCholestroolRiskPoints(vs, person);
        int smokerRisk = person.getPatient().getSmokerRiskPoints(vs, person);
        int diabeticRisk = person.getPatient().getDiabetesRiskPoints(vs, person);
        
        risk = (ageRisk + systBPRisk + hdlRisk + cholestrolRisk + smokerRisk + diabeticRisk);
        
        return risk;
    }
    
}