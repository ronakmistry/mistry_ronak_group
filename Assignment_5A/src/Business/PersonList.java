/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Ronak
 */
public class PersonList {
    
    private ArrayList<Person> personList;
    
    public PersonList()
    {
        personList = new ArrayList<Person>();
    }

    public ArrayList<Person> getPersonList() {
        return personList;
    }

    public void setPersonList(ArrayList<Person> personList) {
        this.personList = personList;
    }
    //Function to add a person
    public Person addPerson(int personID, String name, String gender, int age)
    {
        Person ps = new Person();
        ps.setPersonID(personID);
        ps.setName(name);
        ps.setGender(gender);
        ps.setAge(age);
        personList.add(ps);
        return ps;
    }
    
    //function to remove a person
    public void removePerson(Person ps)
    {
        personList.remove(ps);
    }
}
