/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Surabhi Patil
 */
public class FamilyList {
    
    private ArrayList<Family> familyList;
    
    public FamilyList()
    {
        familyList = new ArrayList<Family>();
        
    }

    public ArrayList<Family> getFamilyList() {
        return familyList;
    }

    public void setFamilyList(ArrayList<Family> familyList) {
        this.familyList = familyList;
    }
    
    //Function to add a family
    public Family addFamily(String fName, int fId)
    {
        Family fy = new Family();
        fy.setfName(fName);
        fy.setfId(fId);
        familyList.add(fy);
        return fy;
    }
    
    public void removeFamily(Family fy)
    {
        familyList.remove(fy);
    }
    

    public boolean highRiskPercentPerson(int id) {
        boolean flag = false;

        for (Family family : familyList) {
            if (family.getfId()==id) {
                flag = true;
                Person highRiskperson = null;
                for (Person person : family.getPersonList().getPersonList()) {
                    if (family.getPersonList().getPersonList().indexOf(person) == 0) {
                        highRiskperson = person;
                    }

                    if (person.getPatient().getRiskPercent() > highRiskperson.getPatient().getRiskPercent()) {
                        highRiskperson = person;
                    }

                }

                System.out.println("\nPerson with highest risk percent in this family is: "
                        + highRiskperson.getName() + " " + highRiskperson.getGender() + " " + highRiskperson.getAge() + " "
                        + highRiskperson.getPatient().getRiskPercent() + "%");
            }
        }

        return flag;
    }

    
    

    public void maxRisk(City city, int id) {
        
        int countDia = 0;
        int countNonDia = 0;
        for (Family family : familyList) {

            if (family.getfId()==id) {
                for (Person person : family.getPersonList().getPersonList()) {
                    VitalSigns vs = person.getPatient().getVsh().getVitalSignHistory().get(4);
                    if (vs.isIsDiabetic()) {
                        countDia++;
                    } else {
                        countNonDia++;
                    }

                }
                //System.out.println(countDia);
                //System.out.println(countNonDia);
                System.out.println("The ratio of diabetic to non diabetic people in the family is :"+countDia+":"+countNonDia);
            }

        }
       
    }
    
}
