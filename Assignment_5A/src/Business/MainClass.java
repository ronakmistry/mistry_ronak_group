/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author Ronak
 */
public class MainClass {
    
    
    public static void main(String[] args)
    {
        City city = new City();
        city.setCityName("Boston");
        init(city);
        //displayResults(city);  
        new MainClass().menuDriven(city);
        
    }
    
    //Function to Initialize User Menu
    public void menuDriven(City city)
    {
        System.out.println("FRAMINGHAM HEART STUDY");
        System.out.println("1. Risk Calculator \n 2.Display reports \n 3.Break");
        Scanner sc = new Scanner(System.in);
        int choice = sc.nextInt();
        switch(choice)
        {
            case 1:
            CalculateRisk calculateRisk = new CalculateRisk();
            System.out.println("Do you wish to continue? \n 1.Yes \n 2.No");
            choice = sc.nextInt();
            if(choice == 1)
            {
                menuDriven(city);
            }
            break; 
            
            case 2:
            reportGeneration(city);
            System.out.println("Do you wish to continue? \n 1.Yes \n 2.No");
            choice = sc.nextInt();
            if(choice == 1)
            {
                menuDriven(city);
            }
            break; 
        }

        
    }
    
    //Function to call report generation on every level
    public void reportGeneration(City city)
    {
        Reports r = new Reports();
        System.out.println("REPORTS");
        System.out.println("1. City \n 2. Community \n 3. House \n 4.Family \n 5.Person \n 6. Back");
        Scanner sc = new Scanner(System.in);
        int choice = sc.nextInt();
        switch (choice)
        {
            case 1:
                r.cityReports(city);
                reportGeneration(city);
                break;
            case 2:
                r.communityReports(city);
                reportGeneration(city);
                break;
            case 3:
                System.out.println("Enter House ID:");
                int houseID=sc.nextInt();
                r.houseReports(city,houseID);
                reportGeneration(city);
                break;
            case 4:
                System.out.println("Enter Family ID:");
                int famID=sc.nextInt();
                r.familyReports(city);
                reportGeneration(city);
                break;
            case 5:
                System.out.println("Enter Person ID:");
                int personID=sc.nextInt();
                r.personReports(city, personID);
                reportGeneration(city);
                break;
            case 6:
                break;
        }
    }
    
    //Init function to initialize data
    public static void init(City city)
    {
        //Count variable used to assign ID's to each person
        int count = 0;
        //Begin for loop for community
        for(int i=0;i<=4;i++)
        {
            Community comm;
            comm = city.getCommunityList().addCommunity(generateString(5), (i+1));
            
            //Begin for loop for House
            for (int j=0;j<=24;j++)
            {
                House house;
                house = comm.getHouseList().addHouse(generateString(6), (j+1));
                
                //Begin for loop for Families within houses
                for (int k=0;k<=1;k++)
                {
                    Family family;
                    family = house.getFamilyList().addFamily(generateString(8), (k+1));
                    
                    //Begin for loop to initialize persons within a family
                    for (int l=0;l<=5;l++)
                    {
                        Person person;
                        Patient patient = new Patient();
                        for (int m=0;m<=4;m++)
                        {
                            patient.getVsh().addVitalSigns(randomInRange(3.6F, 10.3F), randomInRange(0.8F, 4.0F), generateBoolean(), generateBoolean(), generateInt(120, 160), generateBoolean());
                        }
                        
                        //Switch case to handle age distribution within family
                        switch(l)
                        {
                            case 0: person = family.getPersonList().addPerson((count+1), generateString(8), generateGender(), generateInt(65, 80));
                            count++;
                            person.setPatient(patient);
                            patient.setPatientID(count);
                            int risk = person.calculateRiskPointTotal(person);
                            person.getPatient().calculateRisk(risk, person);
                            break;
                            
                            case 1: person = family.getPersonList().addPerson((count+1), generateString(8), generateGender(), generateInt(65, 80));
                            count++;
                            person.setPatient(patient);
                            patient.setPatientID(count);
                            int risk2 = person.calculateRiskPointTotal(person);
                            person.getPatient().calculateRisk(risk2, person);
                            break;
                            
                            case 2: person = family.getPersonList().addPerson((count+1), generateString(8), generateGender(), generateInt(45, 60));
                            count++;
                            person.setPatient(patient);
                            patient.setPatientID(count);
                            int risk3 = person.calculateRiskPointTotal(person);
                            person.getPatient().calculateRisk(risk3, person);
                            break;
                            
                            case 3: person = family.getPersonList().addPerson((count+1), generateString(8), generateGender(), generateInt(45, 60));
                            count++;
                            person.setPatient(patient);
                            patient.setPatientID(count);
                            int risk4 = person.calculateRiskPointTotal(person);
                            person.getPatient().calculateRisk(risk4, person);
                            break;
                            
                            case 4: person = family.getPersonList().addPerson((count+1), generateString(8), generateGender(), generateInt(30, 45));
                            count++;
                            person.setPatient(patient);
                            patient.setPatientID(count);
                            int risk5 = person.calculateRiskPointTotal(person);
                            person.getPatient().calculateRisk(risk5, person);
                            break;
                            
                            case 5: person = family.getPersonList().addPerson((count+1), generateString(8), generateGender(), generateInt(30, 45));
                            count++;
                            person.setPatient(patient);
                            patient.setPatientID(count);
                            int risk6 = person.calculateRiskPointTotal(person);
                            person.getPatient().calculateRisk(risk6, person);
                            break;
                        }
                        
                    }
                }
            }
        }
    }
    
    //Function to generate a random String
    public static String generateString(int length) 
    {
        Random rand = new Random();
        String characters = "QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm";
        char[] text = new char[length];
        for (int i = 0; i < length; i++) {
            text[i] = characters.charAt(rand.nextInt(characters.length()));
        }
        return new String(text);
    }
    
    //Function to generate a random Gender
    public static String generateGender()
    {
        String gender;
        gender = "Male";
        Random rand = new Random();
        int randomNum = rand.nextInt(1);
        
        switch(randomNum)
        {
            case 0: gender = "Male";
            break;
            
            case 1: gender = "Female";
            break;
        }
        return gender;
    }
    
    //Function to generate a random boolean value
    public static boolean generateBoolean()
    {
        boolean boolVal;
        boolVal = true;
        Random rand = new Random();
        int randomNum = rand.nextInt(1);
        
        switch(randomNum)
        {
            case 0: boolVal = true;
            break;
            
            case 1: boolVal = false;
            break;
        }
        return boolVal;
    }
    
    //Function to generate a random Integer
    public static int generateInt(int range1, int range2)
    {
        Random rand = new Random();
        int randomNum = rand.nextInt((range2 - range1) + 1) + range1;

        return randomNum;
    }
    
    //Function to generate a random float value
    public static float randomInRange(float min, float max) 
    {
        Random random = new Random();
        float range = max - min;
        float scale = random.nextFloat() * range;
        float shift = scale + min;
        return shift;
    }
}
    
        

