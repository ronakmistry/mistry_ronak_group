/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Surabhi Patil
 */
public class HouseList {
    private ArrayList<House> houseList;
    
    public HouseList()
    {
        houseList = new ArrayList<House>();
    }

    public ArrayList<House> getHouseList() {
        return houseList;
    }

    public void setHouseList(ArrayList<House> houseList) {
        this.houseList = houseList;
    }
    
    //Function to add a house
    public House addHouse(String hName, int hId)
    {
        House h = new House();
        h.sethName(hName);
        h.sethID(hId);
        houseList.add(h);
        return h;
    }
    
    //Function to remove a house
    public void removeHouse(House h)
    {
        houseList.remove(h);
    }
    
    //Function to check how many diabetic members in a house
    public void hDiabetic(City city, int hoID)
    {
       int count = 0;
       for (Community community : city.getCommunityList().getCommunityList()) {
       for(House house: community.getHouseList().getHouseList())
       {
           if(house.gethID()==hoID)
           {
               for(Family family:house.getFamilyList().getFamilyList())
               {
                   for(Person person:family.getPersonList().getPersonList())
                   {
                       boolean value = person.getPatient().getVsh().getMeanVitals(person).isIsDiabetic();
                       if(value==true)
                       {
                           count++;
                       }
                   }
               }
           }
       }
       }
       System.out.println(count+" number of people in the house are diabetic");
    }
    
    //Function to check members in a house with high cholestrol
    public void hCholesterol(City city, int hoID)
    {
       int count = 0;
       for (Community community : city.getCommunityList().getCommunityList()) {
       for(House house: community.getHouseList().getHouseList())
       {
           if(house.gethID()==hoID)
           {
               for(Family family:house.getFamilyList().getFamilyList())
               {
                   for(Person person:family.getPersonList().getPersonList())
                   {
                       float chol = person.getPatient().getVsh().getMeanVitals(person).getCholesterol();
                       if(chol>=6.21)
                       {
                           count++;
                       }
                   }
               }
           }
       }
       }
       System.out.println(count+" number of people in the house have high cholesterol");
   }
}
