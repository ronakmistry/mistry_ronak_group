/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Surabhi Patil
 */
public class Department {
    private String deptName;
    private int deptId;
    private static int countDept=0;
    private CourseCatalog courseCatalog;
    private CourseOfferingDirectory courseOfferingDirectory;
    private DepartmentStudentDirectory departmentStudentDirectory;
    private TeacherDirectory teacherDirectory;

    
    public TeacherDirectory getTeacherDirectory() {
        return teacherDirectory;
    }

    public void setTeacherDirectory(TeacherDirectory teacherDirectory) {
        this.teacherDirectory = teacherDirectory;
    }

    public DepartmentStudentDirectory getDepartmentStudentDirectory() {
        return departmentStudentDirectory;
    }
    
    
    
    public Department()
    {
        countDept++;
        deptId = countDept;
        courseCatalog = new CourseCatalog();
        courseOfferingDirectory = new CourseOfferingDirectory();
        departmentStudentDirectory = new DepartmentStudentDirectory();
        teacherDirectory = new TeacherDirectory();
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public int getDeptId() {
        return deptId;
    }

    public void setDeptId(int deptId) {
        this.deptId = deptId;
    }

    public CourseCatalog getCourseCatalog() {
        return courseCatalog;
    }

    public CourseOfferingDirectory getCourseOfferingDirectory() {
        return courseOfferingDirectory;
    }
    
}
