/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Surabhi Patil
 */
public class DepartmentCatalog {
    
    private ArrayList<Department> departmentCatalog;
    
    public DepartmentCatalog()
    {
        departmentCatalog =  new ArrayList<Department>();
    }

    public ArrayList<Department> getDepartmentCatalog() {
        return departmentCatalog;
    }

    public Department addDepartment(String name)
    {
        Department d = new Department();
        d.setDeptName(name);
        departmentCatalog.add(d);
        return d;
    }
    
    public void removeDepartment(Department d)
    {
        departmentCatalog.remove(d);
    }
    
    public Department searchDepartment(String name)
    {
        for(Department d:departmentCatalog)
        {
            if(d.getDeptName()==name)
            {
                return d;
            }
        }
        return null;
    }
    
}
