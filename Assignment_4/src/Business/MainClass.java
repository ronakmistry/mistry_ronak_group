/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author Ronak
 */
public class MainClass {
    
    //Begin PSVM
    public static void main(String[] args) {
        
    UniversityStudentDirectory universityStudentDirectory;
    CollegeCatalog collegeCatalog;
    CollegeStudentDirectory collegeStudentDirectory;
    DepartmentCatalog deptCatalog;
    DepartmentStudentDirectory deptStudentDirectory;
    CourseCatalog courseCatalog;
    CourseOfferingDirectory courseOfferingDirectory;
    TeacherDirectory teacherDirectory;
    
    //universityStudentDirectory = new UniversityStudentDirectory();
    //Sem
    ArrayList<Semester> sem = new ArrayList<Semester>();
    Semester sem1 = new Semester();
    sem1.setValue(Semester.Sem.Spring2015);
    sem.add(sem1);
    Semester sem2 = new Semester();
    sem2.setValue(Semester.Sem.Fall2015);
    sem.add(sem2);
    Semester sem3 = new Semester();
    sem3.setValue(Semester.Sem.Spring2016);
    sem.add(sem3);
    Semester sem4 = new Semester();
    sem4.setValue(Semester.Sem.Fall2016);
    sem.add(sem4);
        
        UniversityCatalog uniCatalog = new UniversityCatalog();

        University uniAdd = uniCatalog.addUniv("NEU", "Boston");

        University uniAdd2 = uniCatalog.addUniv("BU", "Boston");

        University uniAdd3 = uniCatalog.addUniv("MIT", "Boston");

        College colAdd1 = uniCatalog.searchUniversity("NEU").getCollegecatalog().addCollege("College of Engineering");
        College colAdd2 = uniCatalog.searchUniversity("NEU").getCollegecatalog().addCollege("NEU School of Business");
        College colAdd3 = uniCatalog.searchUniversity("NEU").getCollegecatalog().addCollege("College of Medicine");

        College colAdd4 = uniCatalog.searchUniversity("BU").getCollegecatalog().addCollege("College of Science");
        College colAdd5 = uniCatalog.searchUniversity("BU").getCollegecatalog().addCollege("College of Economics");
        College colAdd6 = uniCatalog.searchUniversity("BU").getCollegecatalog().addCollege("BU Law School");

        College colAdd7 = uniCatalog.searchUniversity("MIT").getCollegecatalog().addCollege("College of Biology");
        College colAdd8 = uniCatalog.searchUniversity("MIT").getCollegecatalog().addCollege("College of Chemical Technology");
        College colAdd9 = uniCatalog.searchUniversity("MIT").getCollegecatalog().addCollege("College of Physics");

        Department dept1 = uniCatalog.searchUniversity("NEU").getCollegecatalog().searchCollege("College of Engineering").getDepartmentCatalog().addDepartment("Information Systems");
        Department dept2 = uniCatalog.searchUniversity("NEU").getCollegecatalog().searchCollege("College of Engineering").getDepartmentCatalog().addDepartment("Computer Science");

        Department dept3 = uniCatalog.searchUniversity("NEU").getCollegecatalog().searchCollege("NEU School of Business").getDepartmentCatalog().addDepartment("MBA");
        Department dept4 = uniCatalog.searchUniversity("NEU").getCollegecatalog().searchCollege("NEU School of Business").getDepartmentCatalog().addDepartment("MIS");

        Department dept5 = uniCatalog.searchUniversity("NEU").getCollegecatalog().searchCollege("College of Medicine").getDepartmentCatalog().addDepartment("Ophthalmology");
        Department dept6 = uniCatalog.searchUniversity("NEU").getCollegecatalog().searchCollege("College of Medicine").getDepartmentCatalog().addDepartment("Ortho");

        Department dept7 = uniCatalog.searchUniversity("BU").getCollegecatalog().searchCollege("College of Science").getDepartmentCatalog().addDepartment("Mathematics");
        Department dept8 = uniCatalog.searchUniversity("BU").getCollegecatalog().searchCollege("College of Science").getDepartmentCatalog().addDepartment("Information Science");

        Department dept9 = uniCatalog.searchUniversity("BU").getCollegecatalog().searchCollege("College of Economics").getDepartmentCatalog().addDepartment("Supply Chain Management");
        Department dept10 = uniCatalog.searchUniversity("BU").getCollegecatalog().searchCollege("College of Economics").getDepartmentCatalog().addDepartment("Microeconomics");

        Department dept11 = uniCatalog.searchUniversity("BU").getCollegecatalog().searchCollege("BU Law School").getDepartmentCatalog().addDepartment("Criminal Law");
        Department dept12 = uniCatalog.searchUniversity("BU").getCollegecatalog().searchCollege("BU Law School").getDepartmentCatalog().addDepartment("Corporate Law");

        Department dept13 = uniCatalog.searchUniversity("MIT").getCollegecatalog().searchCollege("College of Biology").getDepartmentCatalog().addDepartment("Botany");
        Department dept14 = uniCatalog.searchUniversity("MIT").getCollegecatalog().searchCollege("College of Biology").getDepartmentCatalog().addDepartment("Zoology");

        Department dept15 = uniCatalog.searchUniversity("MIT").getCollegecatalog().searchCollege("College of Chemical Technology").getDepartmentCatalog().addDepartment("Polymers");
        Department dept16 = uniCatalog.searchUniversity("MIT").getCollegecatalog().searchCollege("College of Chemical Technology").getDepartmentCatalog().addDepartment("Petroleum");

        Department dept17 = uniCatalog.searchUniversity("MIT").getCollegecatalog().searchCollege("College of Physics").getDepartmentCatalog().addDepartment("Astronomy");
        Department dept18 = uniCatalog.searchUniversity("MIT").getCollegecatalog().searchCollege("College of Physics").getDepartmentCatalog().addDepartment("Theoretical Physics");

        Course cou1 = uniCatalog.searchUniversity("NEU").getCollegecatalog().searchCollege("College of Engineering").getDepartmentCatalog().searchDepartment("Information Systems").getCourseCatalog().addCourse("AED", 1, 4, 1500, true, null);
        Course cou2 = uniCatalog.searchUniversity("NEU").getCollegecatalog().searchCollege("College of Engineering").getDepartmentCatalog().searchDepartment("Information Systems").getCourseCatalog().addCourse("DBMS", 2, 4, 1500, true, null);
        Course cou3 = uniCatalog.searchUniversity("NEU").getCollegecatalog().searchCollege("College of Engineering").getDepartmentCatalog().searchDepartment("Computer Science").getCourseCatalog().addCourse("Computer System Architecture", 11, 4, 2000, true, null);
        Course cou4 = uniCatalog.searchUniversity("NEU").getCollegecatalog().searchCollege("College of Engineering").getDepartmentCatalog().searchDepartment("Computer Science").getCourseCatalog().addCourse("HCI", 12, 4, 2000, true, null);

        Course cou5 = uniCatalog.searchUniversity("NEU").getCollegecatalog().searchCollege("NEU School of Business").getDepartmentCatalog().searchDepartment("MBA").getCourseCatalog().addCourse("Marketing", 21, 4, 2000, true, null);
        Course cou6 = uniCatalog.searchUniversity("NEU").getCollegecatalog().searchCollege("NEU School of Business").getDepartmentCatalog().searchDepartment("MBA").getCourseCatalog().addCourse("Sales", 22, 4, 2000, true, null);

        Course cou7 = uniCatalog.searchUniversity("NEU").getCollegecatalog().searchCollege("NEU School of Business").getDepartmentCatalog().searchDepartment("MIS").getCourseCatalog().addCourse("Business Process Engineering", 31, 4, 2000, true, null);
        Course cou8 = uniCatalog.searchUniversity("NEU").getCollegecatalog().searchCollege("NEU School of Business").getDepartmentCatalog().searchDepartment("MIS").getCourseCatalog().addCourse("Big Data", 32, 4, 2000, true, null);

        Course cou9 = uniCatalog.searchUniversity("NEU").getCollegecatalog().searchCollege("College of Medicine").getDepartmentCatalog().searchDepartment("Ophthalmology").getCourseCatalog().addCourse("Retina", 41, 4, 2500, true, null);
        Course cou10 = uniCatalog.searchUniversity("NEU").getCollegecatalog().searchCollege("College of Medicine").getDepartmentCatalog().searchDepartment("Ophthalmology").getCourseCatalog().addCourse("Pupil", 42, 4, 2500, true, null);
        Course cou11 = uniCatalog.searchUniversity("NEU").getCollegecatalog().searchCollege("College of Medicine").getDepartmentCatalog().searchDepartment("Ortho").getCourseCatalog().addCourse("Spinal Cord", 51,4,2500,true,null);
        Course cou12 = uniCatalog.searchUniversity("NEU").getCollegecatalog().searchCollege("College of Medicine").getDepartmentCatalog().searchDepartment("Ortho").getCourseCatalog().addCourse("Knee", 52,4,2500,true,null);
    
    
        Course cou13 = uniCatalog.searchUniversity("BU").getCollegecatalog().searchCollege("College of Science").getDepartmentCatalog().searchDepartment("Mathematics").getCourseCatalog().addCourse("Algebra",61,4,2500,true,null);
        Course cou14 = uniCatalog.searchUniversity("BU").getCollegecatalog().searchCollege("College of Science").getDepartmentCatalog().searchDepartment("Mathematics").getCourseCatalog().addCourse("Geometry",61,4,2500,true,null);
        
        
        Course cou15 = uniCatalog.searchUniversity("BU").getCollegecatalog().searchCollege("College of Science").getDepartmentCatalog().searchDepartment("Information Science").getCourseCatalog().addCourse("Data Mining",71,4,2000,true,null);
        Course cou16 = uniCatalog.searchUniversity("BU").getCollegecatalog().searchCollege("College of Science").getDepartmentCatalog().searchDepartment("Information Science").getCourseCatalog().addCourse("Business Intelligence",72,4,2000,true,null);
      
        Course cou17 = uniCatalog.searchUniversity("BU").getCollegecatalog().searchCollege("College of Economics").getDepartmentCatalog().searchDepartment("Supply Chain Management").getCourseCatalog().addCourse("Trade",81,4,2000,true,null);
        Course cou18 = uniCatalog.searchUniversity("BU").getCollegecatalog().searchCollege("College of Economics").getDepartmentCatalog().searchDepartment("Supply Chain Management").getCourseCatalog().addCourse("Financial Market",82,4,2000,true,null);
      
        Course cou19 = uniCatalog.searchUniversity("BU").getCollegecatalog().searchCollege("College of Economics").getDepartmentCatalog().searchDepartment("Microeconomics").getCourseCatalog().addCourse("Uncertainty in Market",91,4,3000,true,null);
        Course cou20 = uniCatalog.searchUniversity("BU").getCollegecatalog().searchCollege("College of Economics").getDepartmentCatalog().searchDepartment("Microeconomics").getCourseCatalog().addCourse("Economic Law",91,4,3000,true,null);
      
        Course cou21 = uniCatalog.searchUniversity("BU").getCollegecatalog().searchCollege("BU Law School").getDepartmentCatalog().searchDepartment("Criminal Law").getCourseCatalog().addCourse("Criminology",101,4,2000,true,null);
              Course cou22 = uniCatalog.searchUniversity("BU").getCollegecatalog().searchCollege("BU Law School").getDepartmentCatalog().searchDepartment("Criminal Law").getCourseCatalog().addCourse("Criminal Psychology",101,4,2000,true,null);
        
        Course cou23 = uniCatalog.searchUniversity("BU").getCollegecatalog().searchCollege("BU Law School").getDepartmentCatalog().searchDepartment("Corporate Law").getCourseCatalog().addCourse("Corporate Finance",111,4,2000,true,null);
               Course cou24 = uniCatalog.searchUniversity("BU").getCollegecatalog().searchCollege("BU Law School").getDepartmentCatalog().searchDepartment("Corporate Law").getCourseCatalog().addCourse("Business Ethics",112,4,2000,true,null);
       
        Course cou25 = uniCatalog.searchUniversity("MIT").getCollegecatalog().searchCollege("College of Biology").getDepartmentCatalog().searchDepartment("Botany").getCourseCatalog().addCourse("Plants",121,4,1000,true,null);
         Course cou26 = uniCatalog.searchUniversity("MIT").getCollegecatalog().searchCollege("College of Biology").getDepartmentCatalog().searchDepartment("Botany").getCourseCatalog().addCourse("Flowers",122,4,1000,true,null);
      
        Course cou27 = uniCatalog.searchUniversity("MIT").getCollegecatalog().searchCollege("College of Biology").getDepartmentCatalog().searchDepartment("Zoology").getCourseCatalog().addCourse("Herbivores",131,4,1000,true,null);
         Course cou28 = uniCatalog.searchUniversity("MIT").getCollegecatalog().searchCollege("College of Biology").getDepartmentCatalog().searchDepartment("Zoology").getCourseCatalog().addCourse("Carnivores",131,4,1000,true,null);
     
        Course cou29 = uniCatalog.searchUniversity("MIT").getCollegecatalog().searchCollege("College of Chemical Technology").getDepartmentCatalog().searchDepartment("Polymers").getCourseCatalog().addCourse("Plastics",141,4,1000,true,null);
       Course cou30 = uniCatalog.searchUniversity("MIT").getCollegecatalog().searchCollege("College of Chemical Technology").getDepartmentCatalog().searchDepartment("Polymers").getCourseCatalog().addCourse("Carbon Theory",142,4,1000,true,null);
      
        Course cou31 = uniCatalog.searchUniversity("MIT").getCollegecatalog().searchCollege("College of Chemical Technology").getDepartmentCatalog().searchDepartment("Petroleum").getCourseCatalog().addCourse("Refining",151,4,2000,true,null);
         Course cou32 = uniCatalog.searchUniversity("MIT").getCollegecatalog().searchCollege("College of Chemical Technology").getDepartmentCatalog().searchDepartment("Petroleum").getCourseCatalog().addCourse("Alternative Fuel",152,4,2000,true,null);
     
       
        Course cou33 = uniCatalog.searchUniversity("MIT").getCollegecatalog().searchCollege("College of Physics").getDepartmentCatalog().searchDepartment("Astronomy").getCourseCatalog().addCourse("Gravitational Force",161,4,2000,true,null);
         Course cou34 = uniCatalog.searchUniversity("MIT").getCollegecatalog().searchCollege("College of Physics").getDepartmentCatalog().searchDepartment("Astronomy").getCourseCatalog().addCourse("Satellite Communication",162,4,2000,true,null);
       
        Course cou35 = uniCatalog.searchUniversity("MIT").getCollegecatalog().searchCollege("College of Physics").getDepartmentCatalog().searchDepartment("Theoretical Physics").getCourseCatalog().addCourse("String Theory",171,4,2000,true,null);
       Course cou36 = uniCatalog.searchUniversity("MIT").getCollegecatalog().searchCollege("College of Physics").getDepartmentCatalog().searchDepartment("Theoretical Physics").getCourseCatalog().addCourse("Dark Matter",172,4,2000,true,null);
     
        
        TeacherDirectory teacher = dept1.getTeacherDirectory();
        Teacher teachAdd1 = teacher.addTeacher("Professor Bugrara", true, true);
        Teacher teachAdd2 = teacher.addTeacher("Professor Sherman", true, true);
        Teacher teachAdd3 = teacher.addTeacher("Professor Ozbek", true, true);
        Teacher teachAdd4 = teacher.addTeacher("Professor Meyer", true, true);
        Teacher teachAdd5 = teacher.addTeacher("Professor Chawla", true, true);
        
        Seat seat1 = new Seat(cou2.getCourseID(), 100, 200);
        Seat seat2 = new Seat(cou1.getCourseID(), 150, 250);
        Seat seat3 = new Seat(cou3.getCourseID(), 60, 100);
       
        Classroom classRoom1 = new Classroom(108, "Snell");
        Classroom classRoom2 = new Classroom(111, "Shilman");
        Classroom classRoom3 = new Classroom(202, "Hayden");
        Classroom classRoom4 = new Classroom(312, "Behrakis");
        
        CourseOffering coOff1 = uniCatalog.searchUniversity("NEU").getCollegecatalog().searchCollege("College of Engineering").getDepartmentCatalog().searchDepartment("Information Systems").getCourseOfferingDirectory().addCourseOffering(cou1, seat1, teachAdd1, classRoom1);
        CourseOffering coOff2 = uniCatalog.searchUniversity("NEU").getCollegecatalog().searchCollege("College of Engineering").getDepartmentCatalog().searchDepartment("Information Systems").getCourseOfferingDirectory().addCourseOffering(cou2, seat2, teachAdd2, classRoom2);
        CourseOffering coOff3 = uniCatalog.searchUniversity("NEU").getCollegecatalog().searchCollege("College of Engineering").getDepartmentCatalog().searchDepartment("Information Systems").getCourseOfferingDirectory().addCourseOffering(cou3, seat3, teachAdd3, classRoom3);
       
        //Add Students
        Student name1 = new Student("Ronak", "Mistry", "email1", uniAdd, colAdd1, dept1, true, true, "Google", 150000, "Developer");
        uniAdd.getUniversityStudentDirectory().addStudent(name1);
        colAdd1.getCollegeStudentDirectory().addStudent(name1);
        dept1.getDepartmentStudentDirectory().addStudent(name1);
        
        Student name2 = new Student("Tom", "Hardy", "email2", uniAdd, colAdd1, dept1, false, false, null, 0, null);
        uniAdd.getUniversityStudentDirectory().addStudent(name2);
        colAdd1.getCollegeStudentDirectory().addStudent(name2);
        dept1.getDepartmentStudentDirectory().addStudent(name2);
        
        Student name3 = new Student("Bruce", "Wayne", "email3", uniAdd, colAdd1, dept1, false, false, null, 0, null);
        uniAdd.getUniversityStudentDirectory().addStudent(name3);
        colAdd1.getCollegeStudentDirectory().addStudent(name3);
        dept1.getDepartmentStudentDirectory().addStudent(name3);
        
        Student name4 = new Student("Peter", "Parker", "email4", uniAdd, colAdd1, dept1, false, false, null, 0, null);
        uniAdd.getUniversityStudentDirectory().addStudent(name4);
        colAdd1.getCollegeStudentDirectory().addStudent(name4);
        dept1.getDepartmentStudentDirectory().addStudent(name4);
        
        Student name5 = new Student("Bruce", "Banner", "email5", uniAdd, colAdd1, dept1, false, false, null, 0, null);
        uniAdd.getUniversityStudentDirectory().addStudent(name5);
        colAdd1.getCollegeStudentDirectory().addStudent(name5);
        dept1.getDepartmentStudentDirectory().addStudent(name5);
        
        Student name6 = new Student("Reed", "Richards", "email6", uniAdd, colAdd2, dept4, false, false, null, 0, null);
        uniAdd.getUniversityStudentDirectory().addStudent(name6);
        colAdd1.getCollegeStudentDirectory().addStudent(name6);
        dept1.getDepartmentStudentDirectory().addStudent(name6);
        
         Student name7= new Student("Charles", "Xavier", "email7", uniAdd, colAdd2, dept4, false, false, null, 0, null);
        uniAdd.getUniversityStudentDirectory().addStudent(name7);
        colAdd1.getCollegeStudentDirectory().addStudent(name7);
        dept1.getDepartmentStudentDirectory().addStudent(name7);
        
        
        Student name8= new Student("Barney", "Stinson", "email8", uniAdd, colAdd2, dept4, false, false, null, 0, null);
        uniAdd.getUniversityStudentDirectory().addStudent(name8);
        colAdd1.getCollegeStudentDirectory().addStudent(name8);
        dept1.getDepartmentStudentDirectory().addStudent(name8);
        
         Student name9= new Student("Joey", "Tribbiani", "email9", uniAdd, colAdd2, dept4, false, false, null, 0, null);
        uniAdd.getUniversityStudentDirectory().addStudent(name9);
        colAdd1.getCollegeStudentDirectory().addStudent(name9);
        dept1.getDepartmentStudentDirectory().addStudent(name9);
        
        Student name10= new Student("Ted", "Mosby", "email10", uniAdd, colAdd2, dept4, false, false, null, 0, null);
        uniAdd.getUniversityStudentDirectory().addStudent(name10);
        colAdd1.getCollegeStudentDirectory().addStudent(name10);
        dept1.getDepartmentStudentDirectory().addStudent(name10);
        
           Student name11= new Student("Jesse", "Pinkman", "email11", uniAdd2, colAdd4, dept8, false, false, null, 0, null);
        uniAdd.getUniversityStudentDirectory().addStudent(name11);
        colAdd1.getCollegeStudentDirectory().addStudent(name11);
        dept1.getDepartmentStudentDirectory().addStudent(name11);
        
                 Student name12= new Student("Walter", "White", "email12", uniAdd2, colAdd4, dept8, false, false, null, 0, null);
        uniAdd.getUniversityStudentDirectory().addStudent(name12);
        colAdd1.getCollegeStudentDirectory().addStudent(name12);
        dept1.getDepartmentStudentDirectory().addStudent(name12);
        
                 Student name13= new Student("Phil", "Dunphy", "email13", uniAdd2, colAdd4, dept8, false, false, null, 0, null);
        uniAdd.getUniversityStudentDirectory().addStudent(name13);
        colAdd1.getCollegeStudentDirectory().addStudent(name13);
        dept1.getDepartmentStudentDirectory().addStudent(name13);
        
                 Student name14= new Student("Claire", "Dunphy", "email14", uniAdd2, colAdd4, dept8, false, false, null, 0, null);
        uniAdd.getUniversityStudentDirectory().addStudent(name14);
        colAdd1.getCollegeStudentDirectory().addStudent(name14);
        dept1.getDepartmentStudentDirectory().addStudent(name14);
        
                 Student name15= new Student("Haley", "Dunphy", "email15", uniAdd2, colAdd4, dept8, false, false, null, 0, null);
        uniAdd.getUniversityStudentDirectory().addStudent(name15);
        colAdd1.getCollegeStudentDirectory().addStudent(name15);
        dept1.getDepartmentStudentDirectory().addStudent(name15);
        
        Student name16= new Student("Alex", "Dunphy", "email16", uniAdd3, colAdd8, dept16, false, false, null, 0, null);
        uniAdd.getUniversityStudentDirectory().addStudent(name16);
        colAdd1.getCollegeStudentDirectory().addStudent(name16);
        dept1.getDepartmentStudentDirectory().addStudent(name16);
        
         Student name17= new Student("Matt", "Mudock", "email17", uniAdd3, colAdd8, dept16, false, false, null, 0, null);
        uniAdd.getUniversityStudentDirectory().addStudent(name17);
        colAdd1.getCollegeStudentDirectory().addStudent(name17);
        dept1.getDepartmentStudentDirectory().addStudent(name17);
        
         Student name18= new Student("Lex", "Luthor", "email18", uniAdd3, colAdd8, dept16, false, false, null, 0, null);
        uniAdd.getUniversityStudentDirectory().addStudent(name18);
        colAdd1.getCollegeStudentDirectory().addStudent(name18);
        dept1.getDepartmentStudentDirectory().addStudent(name18);
        
         Student name19= new Student("Tony", "Stark", "email19", uniAdd3, colAdd8, dept16, false, false, null, 0, null);
        uniAdd.getUniversityStudentDirectory().addStudent(name19);
        colAdd1.getCollegeStudentDirectory().addStudent(name19);
        dept1.getDepartmentStudentDirectory().addStudent(name19);
        
         Student name20= new Student("Wade", "Wilson", "email20", uniAdd3, colAdd8, dept16, false, false, null, 0, null);
        uniAdd.getUniversityStudentDirectory().addStudent(name20);
        colAdd1.getCollegeStudentDirectory().addStudent(name20);
        dept1.getDepartmentStudentDirectory().addStudent(name20);
        
        
        
        
        
        
        MainClass mainClass = new MainClass();
     
        ArrayList<CourseOffering> courseOffering = dept1.getCourseOfferingDirectory().getCourseOfferingDirectory();
        for (Student student : dept1.getDepartmentStudentDirectory().getDeptStudentDirectory()) {
            CourseLoadDirectory curseLoadDirectory = student.getTranscript().getCourseLoadDirectory();
            for (int i = 0; i < 4; i++) {
                CourseLoad courseLoad = curseLoadDirectory.addCourseLoad();
                courseLoad.setSemester(sem.get(i));
                SeatAssignmentDirectory seatAssignmentDirectory = courseLoad.getSeatAssignmentDirectory();
                
                SeatAssignment seatAssignment1 = seatAssignmentDirectory.addSeatAssignment();
                seatAssignment1 = mainClass.registerCourses(seatAssignment1, courseOffering, seatAssignmentDirectory);
                
                SeatAssignment seatAssignment2 = seatAssignmentDirectory.addSeatAssignment();
                seatAssignment2 = mainClass.registerCourses(seatAssignment2, courseOffering, seatAssignmentDirectory);
                
            }
        }
        //Check Department Size for Information Systems
        mainClass.studentSizeDept(dept1);
        //Check Courses taught within College of Engineering
        mainClass.coursesTaught(colAdd1);
        //Check Faculty student ratio for Information Systems Department
        mainClass.facultyRatioDept(dept1);
        //Check Seat Availibility in IS department
        mainClass.seatAvailabilityDept(dept1);
        //College catalog faculty
        CollegeCatalog colCat=uniAdd.getCollegecatalog();
        mainClass.facultyRatioColg(colCat);
        //CourseProfessor
        mainClass.courseProfessor(dept1);
}
    //End PSVM
        public SeatAssignment registerCourses(SeatAssignment seatAssignment, ArrayList<CourseOffering> courseOffering, SeatAssignmentDirectory sDirectory) {
        Random rand = new Random();
        CourseOffering courseOffered = courseOffering.get(rand.nextInt(courseOffering.size() - 1));
        if (sDirectory.getSeatAssignmentDirectory().size() == 1) {
            seatAssignment.setGpa(3);
            seatAssignment.setCourseOffering(courseOffered);
            courseOffered = seatAssignment.getCourseOffering();
            seatAssignment.removeSeat(courseOffered);
        } else if (!sDirectory.getSeatAssignmentDirectory().get(0).getCourseOffering().getCourse().getCourseName().equals(courseOffered.getCourse().getCourseName())) {
            seatAssignment.setGpa(3);
            seatAssignment.setCourseOffering(courseOffered);
            courseOffered = seatAssignment.getCourseOffering();
            seatAssignment.removeSeat(courseOffered);
        } else {
            registerCourses(seatAssignment, courseOffering, sDirectory);
        }
        return seatAssignment;
    }
    public void coursesTaught(College college) {

        DepartmentCatalog dd = college.getDepartmentCatalog();
        ArrayList<Department> dept = dd.getDepartmentCatalog();

        ArrayList<Course> course;
        System.out.println("Department Size");
        System.out.println(dept.size());

        for (Department d : dept) {
            course = d.getCourseCatalog().getCourseCatalog();
            System.out.println(d.getDeptName()+ ":");
            for (Course c : course) {
                System.out.println(c.getCourseName());
            } 
        }
        }
 
       public void facultyRatio(DepartmentCatalog departmentCatalog) {
          for (Department department : departmentCatalog.getDepartmentCatalog()) {
            ArrayList<Student> student = department.getDepartmentStudentDirectory().getDeptStudentDirectory();
            int stuSize = student.size();
            ArrayList<Teacher> teacher = department.getTeacherDirectory().getTeacherDirectory();
            int teaSize = teacher.size();

            System.out.println(stuSize + "    " + teaSize);
            System.out.println("The size of " + department.getDeptName() + " department is: " + (stuSize + teaSize));
            System.out.println("The Student-faculty ratio is:" + (stuSize / teaSize) + " : " + "1");
        }
       }
       
        public void facultyRatioDept(Department dept) {
            ArrayList<Student> student = dept.getDepartmentStudentDirectory().getDeptStudentDirectory();
            int stuSize = student.size();
            ArrayList<Teacher> teacher = dept.getTeacherDirectory().getTeacherDirectory();
            int teaSize = teacher.size();

            System.out.println(stuSize + "    " + teaSize);
            System.out.println("The size of " + dept.getDeptName() + " department is: " + (stuSize + teaSize));
            System.out.println("The Student-faculty ratio is:" + (stuSize / teaSize) + " : " + "1"); 
    }
       
        public void seatAvailability(DepartmentCatalog departmentCatalog) {
        for (Department department : departmentCatalog.getDepartmentCatalog()) {
            ArrayList<CourseOffering> courseOffering = department.getCourseOfferingDirectory().getCourseOfferingDirectory();
            for (CourseOffering c : courseOffering) {
                if (c.getSeat().getSeatsAvailable() > 0) {
                    System.out.println("Course Name :" + c.getCourse().getCourseName());
                    System.out.println("Available Seats :" + c.getSeat().getSeatsAvailable());
                }
            }
        }
    }
        public void seatAvailabilityDept(Department dept) {
            ArrayList<CourseOffering> courseOffering = dept.getCourseOfferingDirectory().getCourseOfferingDirectory();
            for (CourseOffering c : courseOffering) {
                if (c.getSeat().getSeatsAvailable() > 0) {
                    System.out.println("Available Course Name :" + c.getCourse().getCourseName());
                    System.out.println("Available Course Name :" + c.getSeat().getSeatsAvailable());
                }
            }
        
    }

        public void facultyRatioColg(CollegeCatalog collegeCatalog)
        {
            ArrayList<Teacher> teacher = null;
            for (College college : collegeCatalog.getCollegeCatalog())
            {
            ArrayList<Student> student = college.getCollegeStudentDirectory().getColgStudentDirectory();
            int studentSize = student.size();
            for(Department dept:college.getDepartmentCatalog().getDepartmentCatalog())
            {
               teacher = dept.getTeacherDirectory().getTeacherDirectory();
            }
            int teacherSize = teacher.size();   
            

            System.out.println(studentSize + "    " + teacherSize);
            System.out.println("The size of " + college.getCollegeName() + " college is: " + (studentSize + teacherSize));
            System.out.println("The Student-faculty ratio is:" + (studentSize / teacherSize) + " : " + "1");
            }
        }
        
        
        public void courseProfessor(Department department) {
        ArrayList<CourseOffering> courseOfferingList = department.getCourseOfferingDirectory().getCourseOfferingDirectory();
        System.out.println("Professors of Information System Department.");
        for (CourseOffering courseOffering : courseOfferingList) {
            System.out.println(courseOffering.getCourse().getCourseName() + ":" + courseOffering.getTeacher().getTeacherName());
        }
        }
        
        public void courseCatalog(Department department) {
        ArrayList<Course> courseCatalogList = department.getCourseCatalog().getCourseCatalog();
        System.out.println("Course offered Information System Department.");
        for (Course course : courseCatalogList) {
            System.out.println(course.getCourseName() + "/n");
        }
    }
        
        public void courseStatus(Department department) {

        ArrayList<CourseOffering> courseOfferingList = department.getCourseOfferingDirectory().getCourseOfferingDirectory();
        System.out.println("List of Electives and core courses");
        for (CourseOffering courseOffering : courseOfferingList) {
            if (courseOffering.getCourse().isIsCore()) {
                System.out.println(courseOffering.getCourse().getCourseName() + " is core.");
            } else {
                System.out.println(courseOffering.getCourse().getCourseName() + " is elective.");
            }
        }
    }
        
        public void studentSizeDept(Department department) {
        System.out.println("The Current student size for " + department.getDeptName() + " department is : " + department.getDepartmentStudentDirectory().getDeptStudentDirectory().size());
    }
        
    
}