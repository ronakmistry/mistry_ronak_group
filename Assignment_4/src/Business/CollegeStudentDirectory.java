/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Surabhi Patil
 */
public class CollegeStudentDirectory {
    private ArrayList<Student> colgStudentDirectory;
    
    public CollegeStudentDirectory()
    {
        colgStudentDirectory = new ArrayList<Student>();
    }

    public ArrayList<Student> getColgStudentDirectory() {
        return colgStudentDirectory;
    }
    
    public Student addStudent(Student s)
    {
        colgStudentDirectory.add(s);
        return s;
    }
    
    public void removeStudent(Student s)
    {
        colgStudentDirectory.remove(s);
    }
    
    public Student searchStudent(int id)
    {
        for(Student s: colgStudentDirectory)
        {
            if(s.getStudentID()==id)
            {
                return s;
            }
            
        }
        return null;
    }
    
}
