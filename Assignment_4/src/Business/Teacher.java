/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Surabhi Patil
 */
public class Teacher {
    private String teacherName;
    private boolean isPhd;
    private boolean isFullTime;

    public boolean isIsPhd() {
        return isPhd;
    }

    public void setIsPhd(boolean isPhd) {
        this.isPhd = isPhd;
    }

    public boolean isIsFullTime() {
        return isFullTime;
    }

    public void setIsFullTime(boolean isFullTime) {
        this.isFullTime = isFullTime;
    }
    
    

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }
    
    
}
