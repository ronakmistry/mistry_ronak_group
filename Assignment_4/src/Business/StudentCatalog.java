package Business;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.ArrayList;

/**
 *
 * @author Ronak
 */
public class StudentCatalog {
    private ArrayList<Student> studentCatalog;
    
    public StudentCatalog()
    {
        studentCatalog = new ArrayList<Student>();
    }

    public ArrayList<Student> getStudentCatalog() {
        return studentCatalog;
    }
    
    public Student addStudent(Student s)
    {
        studentCatalog.add(s);
        return s;
    }
    
    public void removeStudent(Student stu)
    {
        studentCatalog.remove(stu);
    }
    
    public Student searchStudent(int id)
    {
        for(Student stu: studentCatalog)
        {
            if(stu.getStudentID()== id)
            {
                return stu;
            }
        }
        return null;
    }
    
}
