/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Surabhi Patil
 */
public class Transcript {
    
    private CourseLoadDirectory courseLoadDirectory;
    
    public Transcript()
    {
        courseLoadDirectory = new CourseLoadDirectory();
    }

    public CourseLoadDirectory getCourseLoadDirectory() {
        return courseLoadDirectory;
    }
    
    public double calculateGPA()
    { int count=0;
    double gpa=0;
        for( CourseLoad courseLoad :courseLoadDirectory.getCourseLoadDirectory()){
            gpa+= courseLoad.calculateGpa();
            count++;
        }
        gpa= gpa/count;
        return gpa;
    }
}
