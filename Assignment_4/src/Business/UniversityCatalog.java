/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Surabhi Patil
 */
public class UniversityCatalog {
    
    private ArrayList<University> universityCatalog;
    
    public UniversityCatalog()
    {
        universityCatalog = new ArrayList<University>();
    }

    public ArrayList<University> getUniversityCatalog() {
        return universityCatalog;
    }

    public University addUniv(String uniName, String location)
    {
        University u = new University();
        u.setUnivName(uniName);
        u.setUnivLocation(location);
        universityCatalog.add(u);
        return u;
    }
    
    public void delUniversity(University u)
    {
        universityCatalog.remove(u);
    }
    
    public University searchUniversity(String name)
    {
        for(University u:universityCatalog)
        {
            if(u.getUnivName()==name)
                return u;
        }
        return null;
    }
}
