package Business;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.util.ArrayList;

/**
 *
 * @author Ronak
 */
public class CourseCatalog {
    private ArrayList<Course> courseCatalog;
    
    public CourseCatalog()
    {
        courseCatalog = new ArrayList<Course>();
    }

    public ArrayList<Course> getCourseCatalog() {
        return courseCatalog;
    }
    
    public Course addCourse(String courseName, int ID, int hours, int cost, boolean core, String criteria)
    {
        Course course = new Course();
        course.setCourseCost(cost);
        course.setCourseName(courseName);
        course.setCourseID(ID);
        course.setCreditHours(hours);
        course.setIsCore(core);
        course.setCriteria(criteria);
        courseCatalog.add(course);
        return course;
    }
    
    public void removeCourse(Course course)
    {
        courseCatalog.remove(course);
        
    }
    
    public Course searchCourse(String keyword)
    {
        for(Course c: courseCatalog)
        {
            if(keyword.equals(c.getCourseName()))
            {
                return c;
            }
        }
        return null;
    }
}
