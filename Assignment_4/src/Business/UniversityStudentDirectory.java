/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Surabhi Patil
 */
public class UniversityStudentDirectory {
    private ArrayList<Student> univStudentDirectory;
    
    public UniversityStudentDirectory()
    {
        univStudentDirectory = new ArrayList<Student>();
    }

    public ArrayList<Student> getUnivStudentDirectory() {
        return univStudentDirectory;
    }
    
    public Student addStudent(Student student)
    {
        univStudentDirectory.add(student);
        return student;
    }
    
    public void removeStudent(Student student)
    {
        univStudentDirectory.remove(student);
    }
    
    public Student searchStudent(int id)
    {
        for(Student s: univStudentDirectory)
        {
            if(s.getStudentID()==id)
            {
                return s;
            }
            
        }
        return null;
    }
}
