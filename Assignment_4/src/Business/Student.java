package Business;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.lang.String;

/**
 *
 * @author Ronak
 */
public class Student {
    
    private String firstName;
    private String lastName;
    private int studentID;
    private String email;
    private boolean isGraduate;
    private boolean isEmployed;
    private String employer;
    private int salary;
    private String designation;
    private Transcript transcript;
    private University university;
    private College college;
    private Department department;
    private StudentAccount studentAccount;
    private static int count = 0;
    
    public Student(String fname, String lname, String email, University uni,College coll, Department dept, boolean graduate, boolean employed, String employer, int salary, String desig)
    {
        transcript=new Transcript();
        studentAccount=new StudentAccount();
        this.studentID=count++;
        this.firstName=fname;
        this.lastName=lname;
        this.email=email;
        this.university=uni;
        this.college=coll;
        this.department=dept;
        this.isGraduate=graduate;
        this.isEmployed=employed;
        this.employer=employer;
        this.salary=salary;
        this.designation=desig;
    }

    public boolean isIsGraduate() {
        return isGraduate;
    }

    public void setIsGraduate(boolean isGraduate) {
        this.isGraduate = isGraduate;
    }

    public boolean isIsEmployed() {
        return isEmployed;
    }

    public void setIsEmployed(boolean isEmployed) {
        this.isEmployed = isEmployed;
    }

    public String getEmployer() {
        return employer;
    }

    public void setEmployer(String employer) {
        this.employer = employer;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String Designation) {
        this.designation = Designation;
    }

    public Transcript getTranscript() {
        return transcript;
    }
    
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getStudentID() {
        return studentID;
    }

    public void setStudentID(int studentID) {
        this.studentID = studentID;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    @Override
    public String toString()
    {
        return this.firstName;
    }
}
