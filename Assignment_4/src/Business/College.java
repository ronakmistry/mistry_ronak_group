/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Surabhi Patil
 */
public class College {
    private String collegeName;
    private int collegeId;
    private static int countColg=0;
    private DepartmentCatalog departmentCatalog;
    private CollegeStudentDirectory collegeStudentDirectory;
    
    public College()
    {
        countColg++;
        collegeId = countColg;
        departmentCatalog = new DepartmentCatalog();
        collegeStudentDirectory = new CollegeStudentDirectory();
    }

    public CollegeStudentDirectory getCollegeStudentDirectory() {
        return collegeStudentDirectory;
    }

    public void setCollegeStudentDirectory(CollegeStudentDirectory collegeStudentDirectory) {
        this.collegeStudentDirectory = collegeStudentDirectory;
    }

    public String getCollegeName() {
        return collegeName;
    }

    public void setCollegeName(String collegeName) {
        this.collegeName = collegeName;
    }

    public int getCollegeId() {
        return collegeId;
    }

    public DepartmentCatalog getDepartmentCatalog() {
        return departmentCatalog;
    }
    
    
}
