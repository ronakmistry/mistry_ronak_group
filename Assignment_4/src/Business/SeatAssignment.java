/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Surabhi Patil
 */
public class SeatAssignment {
    
    private String studentGrade;
    private float courseCost;
    private double gpa;
    private CourseOffering courseOffering;

    public String getStudentGrade() {
        return studentGrade;
    }

    public void setStudentGrade(String studentGrade) {
        this.studentGrade = studentGrade;
    }

    public float getCourseCost() {
        return courseCost;
    }

    public void setCourseCost(float courseCost) {
        this.courseCost = courseCost;
    }

    /**
     * @return the courseOffering
     */
    public CourseOffering getCourseOffering() {
        return courseOffering;
    }

    /**
     * @param courseOffering the courseOffering to set
     */
    public void setCourseOffering(CourseOffering courseOffering) {
        this.courseOffering = courseOffering;
    }

    /**
     * @return the gpa
     */
    public double getGpa() {
        return gpa;
    }

    /**
     * @param gpa the gpa to set
     */
    public void setGpa(double gpa) {
        this.gpa = gpa;
    }
    
    public int calculateCost()
    {
        int cost;
        int hours;
        int total;
        cost=courseOffering.getCourse().getCourseCost();
        hours=courseOffering.getCourse().getCreditHours();
        total=cost*hours;
        return total;
    }
    
    public void removeSeat(CourseOffering co)
    {
        int availability = co.getSeat().getSeatsAvailable();
        availability = availability - 1;
        co.getSeat().setSeatsAvailable(availability);
    }
    
}
