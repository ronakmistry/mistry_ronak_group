/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Surabhi Patil
 */
public class CollegeCatalog {
    
    private ArrayList<College> collegeCatalog;
    
    public CollegeCatalog()
    {
        collegeCatalog = new ArrayList<College>();
    }

    public ArrayList<College> getCollegeCatalog() {
        return collegeCatalog;
    }
    
    public College addCollege(String collName)
    {
        College c = new College();
        c.setCollegeName(collName);
        collegeCatalog.add(c);
        return c;
    }
    
    public void removeCollege(College c)
    {
        collegeCatalog.remove(c);
    }
    
    public College searchCollege(String name)
    {
        for(College c:collegeCatalog)
        {
            if(c.getCollegeName() == name)
                return c;
        }
        return null;
    }
}
