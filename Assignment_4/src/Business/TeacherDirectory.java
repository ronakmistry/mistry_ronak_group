/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Surabhi Patil
 */
public class TeacherDirectory {
    private ArrayList<Teacher> teacherDirectory;
    
    public TeacherDirectory()
    {
        teacherDirectory = new ArrayList<Teacher>();
    }

    public ArrayList<Teacher> getTeacherDirectory() {
        return teacherDirectory;
    }
    
    public Teacher addTeacher(String name, boolean pHd, boolean fullTime)
    {
        Teacher t = new Teacher();
        t.setTeacherName(name);
        t.setIsPhd(pHd);
        t.setIsFullTime(fullTime);
        teacherDirectory.add(t);
        return t;
    }
    
    public void removeTeacher(Teacher t)
    {
        teacherDirectory.remove(t);
    }        
        
    public Teacher searchTeacher(String teacher)
    {
        for(Teacher t:teacherDirectory)
        {
            if(teacher.equals(t.getTeacherName()))
            {
                return t;
            }
        }
        return null;
    }
    
}


