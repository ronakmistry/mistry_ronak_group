/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Surabhi Patil
 */
public class CourseLoad {
    private Semester semester;
    private SeatAssignmentDirectory seatAssignmentDirectory;

public CourseLoad()
{
    semester = new Semester();
    seatAssignmentDirectory = new SeatAssignmentDirectory();
}

    public Semester getSemester() {
        return semester;
    }

    public void setSemester(Semester semester) {
        this.semester = semester;
    }

    public SeatAssignmentDirectory getSeatAssignmentDirectory() {
        return seatAssignmentDirectory;
    }

    public double calculateGpa(){
    int count=0;
    double gpa=0;
    for(SeatAssignment seatAssignment : seatAssignmentDirectory.getSeatAssignmentDirectory()){
        gpa += seatAssignment.getGpa();
        count ++;
    }
    gpa = gpa/count;
    return  gpa;
}
    
    public int checkAvailability(CourseOffering courseOffering)
    {
        for(CourseOffering courseOffered: semester.getCourseOfferingDirectory().getCourseOfferingDirectory())
        {
            if(courseOffered==courseOffering && courseOffered.getSeat().getSeatsAvailable()>0)
            {
                return courseOffered.getSeat().getSeatsAvailable();
            }
        }
        return 0;
    }

}


