/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Surabhi Patil
 */
public class SeatAssignmentDirectory {
    
    private ArrayList<SeatAssignment> seatAssignmentDirectory;
    
    public SeatAssignmentDirectory()
    {
        seatAssignmentDirectory = new ArrayList<SeatAssignment>();
    }

    public ArrayList<SeatAssignment> getSeatAssignmentDirectory() {
        return seatAssignmentDirectory;
    }
    
    public SeatAssignment addSeatAssignment()
    {
        SeatAssignment sa = new SeatAssignment();
        seatAssignmentDirectory.add(sa);
        return sa;
    }
    
    public void removeSeatAssignment(SeatAssignment sa)
    {
        seatAssignmentDirectory.remove(sa);
    }
    
    
}
