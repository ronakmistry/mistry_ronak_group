/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Surabhi Patil
 */
public class University {
    private String univName;
    private int univId;
    private String univLocation;
    private static int countUniv = 0;
    private CollegeCatalog collegecatalog;
    private UniversityStudentDirectory universityStudentDirectory;
    
   
    public UniversityStudentDirectory getUniversityStudentDirectory() {
        return universityStudentDirectory;
    }

    public void setUniversityStudentDirectory(UniversityStudentDirectory universityStudentDirectory) {
        this.universityStudentDirectory = universityStudentDirectory;
    }
    
    
    public University()
    {
        countUniv++;
        univId = countUniv;
        collegecatalog = new CollegeCatalog();
        universityStudentDirectory= new UniversityStudentDirectory();
    }

    public String getUnivName() {
        return univName;
    }

    public void setUnivName(String univName) {
        this.univName = univName;
    }

    public int getUnivId() {
        return univId;
    }

    public void setUnivId(int univId) {
        this.univId = univId;
    }

    public String getUnivLocation() {
        return univLocation;
    }

    public void setUnivLocation(String univLocation) {
        this.univLocation = univLocation;
    }

    public void setCollegecatalog(CollegeCatalog collegecatalog) {
        this.collegecatalog = collegecatalog;
    }

    public CollegeCatalog getCollegecatalog() {
        return collegecatalog;
    }
    
    
    
    
    
    
    
    
    
    
    
    
}


