/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;
import java.util.Date;
/**
 *
 * @author Surabhi Patil
 */
public class Semester {
    
    private Sem value;
    private CourseOfferingDirectory courseOfferingDirectory;
    
    public Semester()
    {
        courseOfferingDirectory=new CourseOfferingDirectory();
    }
    public CourseOfferingDirectory getCourseOfferingDirectory() {
        return courseOfferingDirectory;
    }

    public Sem getValue() {
        return value;
    }

    public void setValue(Sem value) {
        this.value = value;
    }


   
    public enum Sem {
        
    Spring2015(2015,new Date(01/01/2015),new Date(01/01/2015)),
    Fall2015(2015,new Date(01/01/2015),new Date(01/01/2015)),
    Spring2016(2016,new Date(01/01/2015),new Date(01/01/2015)),
    Fall2016(2016,new Date(01/01/2015),new Date(01/01/2015));    



    private final int year;
    private final Date startDate;
    private final Date endDate;

    private Sem(int year, Date startDate, Date endDate) {
        this.year = year;
        this.startDate = startDate;
        this.endDate = startDate;
    }

        public int getYear() {
            return year;
        }

        public Date getStartDate() {
            return startDate;
        }

        public Date getEndDate() {
            return endDate;
        }
}
    
}
