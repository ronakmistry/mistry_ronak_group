/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Surabhi Patil
 */
public class StudentAccount {
   
    
    public int calculateCost(Student s)
    {
        int courseCost=0;
      
        for(CourseLoad courseLoad: s.getTranscript().getCourseLoadDirectory().getCourseLoadDirectory()){
            for(SeatAssignment seatAssignment: courseLoad.getSeatAssignmentDirectory().getSeatAssignmentDirectory()){
                courseCost+= seatAssignment.calculateCost();
            }
        }
       return courseCost;
    }
}
