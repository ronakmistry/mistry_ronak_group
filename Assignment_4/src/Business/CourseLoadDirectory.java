/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Surabhi Patil
 */
public class CourseLoadDirectory {
    
    private ArrayList<CourseLoad> courseLoadDirectory;
    
    public CourseLoadDirectory()
    {
        courseLoadDirectory = new ArrayList<CourseLoad>();
    }
    
    public CourseLoad addCourseLoad()
    {
        CourseLoad cl = new CourseLoad();
        getCourseLoadDirectory().add(cl);
        return cl;
    }
    
    public void removeCourseLoad(CourseLoad cl)
    {
        getCourseLoadDirectory().remove(cl);
    }

    /**
     * @return the courseLoadDirectory
     */
    public ArrayList<CourseLoad> getCourseLoadDirectory() {
        return courseLoadDirectory;
    }

    /**
     * @param courseLoadDirectory the courseLoadDirectory to set
     */
    public void setCourseLoadDirectory(ArrayList<CourseLoad> courseLoadDirectory) {
        this.courseLoadDirectory = courseLoadDirectory;
    }
    
}
