/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Surabhi Patil
 */
public class DepartmentStudentDirectory {
    private ArrayList<Student> deptStudentDirectory;
    
    public DepartmentStudentDirectory()
    {
        deptStudentDirectory = new ArrayList<Student>();
    }

    public ArrayList<Student> getDeptStudentDirectory() {
        return deptStudentDirectory;
    }
    
    public Student addStudent(Student s)
    {
        deptStudentDirectory.add(s);
        return s;
    }
    
    public void removeStudent(Student s)
    {
        deptStudentDirectory.remove(s);
    }
    
    public Student searchStudent(int id)
    {
        for(Student s: deptStudentDirectory)
        {
            if(s.getStudentID()==id)
            {
                return s;
            }
            
        }
        return null;
    }
}
