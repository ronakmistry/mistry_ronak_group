/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Surabhi Patil
 */

public class CourseOfferingDirectory {
    
    private ArrayList<CourseOffering> courseOfferingDirectory;
    
    public CourseOfferingDirectory()
    {
        courseOfferingDirectory = new ArrayList<CourseOffering>();
    }

    public ArrayList<CourseOffering> getCourseOfferingDirectory() {
        return courseOfferingDirectory;
    }
    
    public CourseOffering addCourseOffering(Course courseName, Seat seat, Teacher teacherName, Classroom classRoom)
    {
        CourseOffering co = new CourseOffering();
        
        co.setCourse(courseName);
        co.setClassroom(classRoom);
        co.setSeat(seat);
        co.setTeacher(teacherName);
        courseOfferingDirectory.add(co);
        return co;
    }
    
    public void removeCourseOffering(CourseOffering co)
    {
        courseOfferingDirectory.remove(co);
    }
    
    public CourseOffering searchCourseOffering(Course keyword)
    {
        for (CourseOffering co:courseOfferingDirectory)
        {
            if(co.getCourse() == keyword)
            {
                return co;
            }
        }
        return null;
    }
            
}
